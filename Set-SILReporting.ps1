﻿$test = Test-Path -Path C:\etc\SILA
$certurl = "https://atscloud247.blob.core.windows.net/sila/sila-auth.atscloud247.com_311016.pfx"
$certtp = "b122e81b7553e91b21f5a6a20b5c0570ea2de324"
$rebootpending = Test-Path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\PendingFileRenameOperations"
$PSVer = ($PSVersionTable.PSVersion.Major).ToString()

#Adapted from https://gist.github.com/altrive/5329377
#Based on <http://gallery.technet.microsoft.com/scriptcenter/Get-PendingReboot-Query-bdb79542>
function Test-PendingReboot
{
    if (Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Component Based Servicing\RebootPending" -EA Ignore) { return $true }
    if (Get-Item "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\RebootRequired" -EA Ignore) { return $true }
    if (Get-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager" -Name PendingFileRenameOperations -EA Ignore) { return $true }
    try
    { 
        $util = [wmiclass]"\\.\root\ccm\clientsdk:CCM_ClientUtilities"
        $status = $util.DetermineIfRebootPending()
        if (($status -ne $null) -and $status.RebootPending)
        {
            return $true
        }
        else
        {
            if ($rebootpending) #Additonal reboot test due to legacy systems
            {
                return $true
            }
        }
    }
    catch
    {
        #Catch errors
        Write-Warning "[CATCH-PR] Errors found during attempt:`n$_"
    }
    return $false
}

function Test-PendingRebootLeg #Used for PS version 2 and 3
{
    if (Get-ChildItem "HKLM:\Software\Microsoft\Windows\CurrentVersion\Component Based Servicing\RebootPending" -EA SilentlyContinue) { return $true }
    if (Get-Item "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\Auto Update\RebootRequired" -EA SilentlyContinue) { return $true }
    if (Get-ItemProperty "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager" -Name PendingFileRenameOperations -EA SilentlyContinue) { return $true }
    try
    { 
        $util = [wmiclass]"\\.\root\ccm\clientsdk:CCM_ClientUtilities"
        $status = $util.DetermineIfRebootPending()
        if (($status -ne $null) -and $status.RebootPending)
        {
            return $true
        }
        else
        {
            if ($rebootpending) #Additonal reboot test due to legacy systems
            {
                return $true
            }
        }
    }
    catch
    {
        #Catch errors
        Write-Warning "[CATCH-PR] Errors found during attempt:`n$_"
    }
    return $false
}

try
{
    if ($PSVer -eq "2" -or $PSVer -eq "3")
    {
        $certcheck = (Get-ChildItem cert:\LocalMachine\My | Select Thumbprint)
        $certcheck2 = $certcheck | Where { $_.Thumbprint -eq $certtp } | Foreach { $_.Thumbprint }
        if ($certcheck2)
        {
            $expolicy = Get-ExecutionPolicy
            Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force
            write-host "Certificate with thumbprint $certtp exists, assuming resumed install!"
            Set-SilLogging –TargetUri "https://sila.atscloud247.com" –CertificateThumbprint $certtp
            Start-SilLogging

            try
            {
                ([Net.DNS]::GetHostEntry("sila.atscloud247.com"))
            }
            catch
            {
                $dnscheck = "Fail"
            }
            
            if($dnscheck -ne "Fail")
            {
                write-host "Publishing data to SILA"
                Publish-SilData
            }
            else
            {
                write-host "Check your DNS settings as this node cannot find sila.atscloud247.com"
            }
            Set-ExecutionPolicy -ExecutionPolicy $expolicy -Force
        }
        else
        {
            $expolicy = Get-ExecutionPolicy
            Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force

            if (-Not $test) #Checks to make sure the directory does not exist, if if does not, we will create it
            {
                write-host "Path Does Not Exist, creating SILA folder under C:\etc"
                New-Item -ItemType Directory -Force -Path C:\etc\SILA
            }

            write-host "Downloading sila-auth.atscloud247.com certificate and installing."
            (New-Object System.Net.WebClient).DownloadFile("http://atscloud247.blob.core.windows.net/sila/sila-auth.atscloud247.com_311016.pfx","c:\etc\SILA\sila-auth.atscloud247.com_311016.pfx")

            write-host "Checking Windows version..."
            $OSVersion = (Get-WmiObject win32_operatingsystem).version
            if ($OSVersion -like "6.3.*") #6.3.* is Windows Server 2012 R2
            {
                write-host "Found Windows Server 2012 R2!"
                (New-Object System.Net.WebClient).DownloadFile("http://atscloud247.blob.core.windows.net/sila/Windows8.1-KB3000850-x64.msu","c:\etc\SILA\Windows8.1-KB3000850-x64.msu")
                (New-Object System.Net.WebClient).DownloadFile("http://atscloud247.blob.core.windows.net/sila/Windows8.1-KB3060681-x64.msu","c:\etc\SILA\Windows8.1-KB3060681-x64.msu")
                wusa C:\etc\SILA\Windows8.1-KB3000850-x64.msu /quiet /norestart
                wusa C:\etc\SILA\Windows8.1-KB3060681-x64.msu /quiet /norestart
                certutil -importpfx -p "u3C6aP2J8q3JX5Z" c:\etc\SILA\sila-auth.atscloud247.com_311016.pfx
            }
            elseif ($OSVersion -like "6.2.*") #6.2.* is Windows Server 2012
            {
                write-host "Found Windows Server 2012!"
                (New-Object System.Net.WebClient).DownloadFile("http://atscloud247.blob.core.windows.net/sila/W2K12-KB3134759-x64.msu","c:\etc\SILA\W2K12-KB3134759-x64.msu")
                wusa C:\etc\SILA\W2K12-KB3134759-x64.msu /quiet /norestart
                certutil -importpfx -p "u3C6aP2J8q3JX5Z" c:\etc\SILA\sila-auth.atscloud247.com_311016.pfx
            }
            elseif ($OSVersion -like "6.1.*") #6.1.* is Windows Server 2008 R2
            {
                write-host "Found Windows Server 2008 Rw!"
                (New-Object System.Net.WebClient).DownloadFile("http://atscloud247.blob.core.windows.net/sila/Win7AndW2K8R2-KB3134760-x64.msu","c:\etc\SILA\Win7AndW2K8R2-KB3134760-x64.msu")
                wusa C:\etc\SILA\Win7AndW2K8R2-KB3134760-x64.msu /quiet /norestart
                certutil -importpfx -p "u3C6aP2J8q3JX5Z" c:\etc\SILA\sila-auth.atscloud247.com_311016.pfx
            }
            else
            {
                #Unsupported OS
                write-host "Unsupported operating system detected, exiting."
                exit
            }

            write-host "Testing if this node needs to be rebooted"
            $reboot = Test-PendingRebootLeg

            if (-Not $reboot) #System does not need a reboot
            {
                write-host "No reboot needed, proceeding to configure SILA"
                Set-SilLogging –TargetUri "https://sila.atscloud247.com" –CertificateThumbprint $certtp
                Start-SilLogging
                Publish-SilData
                Set-ExecutionPolicy -ExecutionPolicy $expolicy -Force
            }
            else
            {
                Set-ExecutionPolicy -ExecutionPolicy $expolicy -Force
                write-host "Host needs to be rebooted!"
                write-host "Afterwards re-run this script to continue configuration."
            }
        }
    }
    else
    {
        write-host "Checking if sila-auth.atscloud247.com exists"
        $certcheck = (Get-ChildItem cert:\LocalMachine\My | Select Thumbprint)
        $certcheck2 = $certcheck | Where { $_.Thumbprint -eq $certtp } | Foreach { $_.Thumbprint }
        if ($certcheck2)
        {
            write-host "Certificate with thumbprint $certtp exists, assuming resumed install!"
            $expolicy = Get-ExecutionPolicy
            Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force
            Set-SilLogging –TargetUri "https://sila.atscloud247.com" –CertificateThumbprint $certtp
            Start-SilLogging
            try
            {
                ([Net.DNS]::GetHostEntry("sila.atscloud247.com"))
            }
            catch
            {
                $dnscheck = "Fail"
            }
            
            if($dnscheck -ne "Fail")
            {
                write-host "Publishing data to SILA"
                Publish-SilData
            }
            else
            {
                write-host "Check your DNS settings as this node cannot find sila.atscloud247.com"
            }
            Set-ExecutionPolicy -ExecutionPolicy $expolicy -Force
        }
        else
        {
            $expolicy = Get-ExecutionPolicy
            Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Force

            if (-Not $test) #Checks to make sure the directory does not exist, if if does not, we will create it
            {
                #write-host "Path Does Not Exist"
                New-Item -ItemType Directory -Force -Path C:\etc\SILA
            }

            Invoke-WebRequest -Uri $certurl -OutFile c:\etc\SILA\sila-auth.atscloud247.com_311016.pfx

            $OSVersion = (Get-WmiObject win32_operatingsystem).version
            if ($OSVersion -like "6.3.*") #6.3.* is Windows Server 2012 R2
            {
                Invoke-WebRequest -Uri "https://atscloud247.blob.core.windows.net/sila/Windows8.1-KB3000850-x64.msu" -OutFile c:\etc\SILA\Windows8.1-KB3000850-x64.msu
                Invoke-WebRequest -Uri "https://atscloud247.blob.core.windows.net/sila/Windows8.1-KB3060681-x64.msu" -OutFile c:\etc\SILA\Windows8.1-KB3060681-x64.msu
                wusa C:\etc\SILA\Windows8.1-KB3000850-x64.msu /quiet /norestart
                wusa C:\etc\SILA\Windows8.1-KB3060681-x64.msu /quiet /norestart
                certutil -importpfx -p "u3C6aP2J8q3JX5Z" c:\etc\SILA\sila-auth.atscloud247.com_311016.pfx
            }
            elseif ($OSVersion -like "6.2.*") #6.2.* is Windows Server 2012
            {
                Invoke-WebRequest -Uri "https://atscloud247.blob.core.windows.net/sila/W2K12-KB3134759-x64.msu" -OutFile c:\etc\SILA\W2K12-KB3134759-x64.msu
                wusa C:\etc\SILA\W2K12-KB3134759-x64.msu /quiet /norestart
                certutil -importpfx -p "u3C6aP2J8q3JX5Z" c:\etc\SILA\sila-auth.atscloud247.com_311016.pfx
            }
            elseif ($OSVersion -like "6.1.*") #6.1.* is Windows Server 2008 R2
            {
                Invoke-WebRequest -Uri "https://atscloud247.blob.core.windows.net/sila/Win7AndW2K8R2-KB3134760-x64.msu" -OutFile c:\etc\SILA\Win7AndW2K8R2-KB3134760-x64.msu
                wusa C:\etc\SILA\Win7AndW2K8R2-KB3134760-x64.msu /quiet /norestart
                certutil -importpfx -p "u3C6aP2J8q3JX5Z" c:\etc\SILA\sila-auth.atscloud247.com_311016.pfx
            }
            else
            {
                #Unsupported OS
                exit
            }

            $reboot = Test-PendingReboot

            if (-Not $reboot) #System does not need a reboot
            {
                Set-SilLogging –TargetUri "https://sila.atscloud247.com" –CertificateThumbprint $certtp
                Start-SilLogging
                Publish-SilData
                Set-ExecutionPolicy -ExecutionPolicy $expolicy -Force
            }
            else
            {
                Set-ExecutionPolicy -ExecutionPolicy $expolicy -Force
                write-host "Host need to be rebooted!"
                write-host "Afterwards re-run this script to continue configuration."
            }
        }
    }
}
catch
{
    #Catch errors
    Write-Warning "[CATCH] Errors found during attempt:`n$_"
}