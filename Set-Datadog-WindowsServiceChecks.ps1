﻿#########################################################
###### DataDog - Windows Service Monitoring Setup #######
#########################################################
### This script automatically configurs the DataDog   ###
### Agent configuration file for windows_service.d    ###
### to monitoring all services under the local system ###
### which are marked for Automatic startup.           ###
#########################################################
### Rerunning the script will automatically update    ###
### the list of services to monitor based on changes  ###
### made to the system.                               ###
#########################################################
### It is recommeneded to run this script at minimum  ###
### once per 12 hours and no more than once per hour. ###
#########################################################
### The settings in the file assume you are running   ###
### DataDog Agent version 6 or above.                 ###
#########################################################

# Chack if DataDog Agent version 6 is installed
$DGAgentVersion = Get-WmiObject -Class Win32_Product | Where Name -eq "Datadog Agent" | ForEach {$_.Version}

if ($DGAgentVersion -lt 6)
{
    #Do nothing because an older agent is installed
    exit
}
else
{
    # Set file name
    $File = 'C:\ProgramData\Datadog\conf.d\windows_service.d\conf.yaml.example'
    $outFile = 'C:\ProgramData\Datadog\conf.d\windows_service.d\conf.yaml'
    $searchTest = '        services:'

    # Get all services which have the startup type set to Automatic
    $Services = Get-Service | Select -Property * | Where StartType -eq "Automatic" | ForEach { $_.Name }

    # Process lines of text from file and assign result to $NewContent variable
    $NewContent = Get-Content -Path $File |
        ForEach-Object {
            # If line matches regex
            if($_ -match ('^' + [regex]::Escape($searchTest)))
            {
                # Output this line to pipeline
                $_

                # And output additional line right after it
                ForEach ($Service in $Services)
                {
                    '          - ' + $Service
                }
            }
            else # If line doesn't matches regex
            {
                # Output this line to pipeline
                $_
            }
        }

    # Write content of $NewContent varibale back to file
    $NewContent | Out-File -FilePath $outFile -Encoding Default -Force

    #Restart the DataDog Agent to make the changes active
    Restart-Service -Name DatadogAgent -Force
}