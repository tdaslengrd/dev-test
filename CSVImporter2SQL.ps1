﻿# Database variables
$SQLServer = "localhost"
$SQLDBName = "testdb"
$table = "testtable"
$uid ="netflowpsuser"
$pwd = "Passw0rd"
$connectionstring = "Data Source=$SQLServer;Initial Catalog=$SQLDBName; Integrated Security = False; User ID = $uid; Password = $pwd;"

# CSV variables
$csvfile = "E:\FileZilla Server\netflowdata\2017-04-01\netflow_all_01_04_2017_0_ftp2_1000Flanders01.csv"
$csvdelimiter = ","
$firstRowColumnNames = $true

$createTable = @"
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[$TableName]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[$table]
	(
        [Id] [int] IDENTITY(1,1) NOT NULL,
		[sIP] [varchar](50) NULL,
		[dIP] [varchar](50) NULL,
		[sPort] [varchar](5) NULL,
		[dPort] [varchar](5) NULL,
		[sTime] [datetime] NULL,
		[eTime] [datetime] NULL,
        [bytes] [bigint] NULL,
	)
END;
"@

Invoke-Sqlcmd -Username $uid -Password $pwd -ServerInstance $SQLServer -Database $SQLDBName -Query $createTable

################### No need to modify anything below ###################
Write-Host "Script started..."
$elapsed = [System.Diagnostics.Stopwatch]::StartNew() 
[void][Reflection.Assembly]::LoadWithPartialName("System.Data")
[void][Reflection.Assembly]::LoadWithPartialName("System.Data.SqlClient")
 
# 50k worked fastest and kept memory usage to a minimum
$batchsize = 50000
 
# Build the sqlbulkcopy connection, and set the timeout to infinite
$bulkcopy = New-Object Data.SqlClient.SqlBulkCopy($connectionstring, [System.Data.SqlClient.SqlBulkCopyOptions]::TableLock)
$bulkcopy.DestinationTableName = $table
$bulkcopy.bulkcopyTimeout = 0
$bulkcopy.batchsize = $batchsize
 
# Create the datatable, and autogenerate the columns.
$datatable = New-Object System.Data.DataTable
 
# Open the text file from disk
$reader = New-Object System.IO.StreamReader($csvfile)
$columns = (Get-Content $csvfile -First 1).Split($csvdelimiter)
if ($firstRowColumnNames -eq $true) { $null = $reader.readLine() }
 
#foreach ($column in $columns) { 
# $null = $datatable.Columns.Add()
#}

foreach ($column in $columns) { 
    #This is specifically added to set the column fields for sTime and eTime to DataTime
    if ($column -eq 'sTime')
    {
        $columninfo = New-Object System.Data.DataColumn $column,([DateTime])
        $null = $datatable.Columns.Add($columninfo)
        write-host "Set $column to DateTime"
    }
    elseif ($column -eq 'eTime')
    {
        $columninfo = New-Object System.Data.DataColumn $column,([DateTime])
        $null = $datatable.Columns.Add($columninfo)
        write-host "Set $column to DateTime"
    }
    elseif ($column -eq 'bytes')
    {
        $columninfo = New-Object System.Data.DataColumn $column,([Int64])
        $null = $datatable.Columns.Add($columninfo)
        write-host "Set $column to Int64"
    }
    else
    {
        $columninfo = New-Object System.Data.DataColumn $column,([String])
	    $null = $datatable.Columns.Add($columninfo)
        write-host "Set $column as String"
    }
}

$bulkcopy.ColumnMappings.Add("sIP", "sIP") | Out-Null
$bulkcopy.ColumnMappings.Add("dIP", "dIP") | Out-Null
$bulkcopy.ColumnMappings.Add("sPort", "sPort") | Out-Null
$bulkcopy.ColumnMappings.Add("dPort", "dPort") | Out-Null
$bulkcopy.ColumnMappings.Add("sTime", "sTime") | Out-Null
$bulkcopy.ColumnMappings.Add("eTime", "eTime") | Out-Null
$bulkcopy.ColumnMappings.Add("bytes", "bytes") | Out-Null

# Read in the data, line by line
while (($line = $reader.ReadLine()) -ne $null)
{
    $null = $datatable.Rows.Add($line.Split($csvdelimiter))
    $i++; if (($i % $batchsize) -eq 0)
    { 
        $bulkcopy.WriteToServer($datatable)
        Write-Host "$i rows have been inserted in $($elapsed.Elapsed.ToString())."
        $datatable.Clear()
    } 
} 
 
# Add in all the remaining rows since the last clear
if($datatable.Rows.Count -gt 0) {
 $bulkcopy.WriteToServer($datatable)
 $datatable.Clear()
}
 
# Clean Up
$reader.Close(); $reader.Dispose()
$bulkcopy.Close(); $bulkcopy.Dispose()
$datatable.Dispose()
 
Write-Host "Script complete. $i rows have been inserted into the database."
Write-Host "Total Elapsed Time: $($elapsed.Elapsed.ToString())"
# Sometimes the Garbage Collector takes too long to clear the huge datatable.
[System.GC]::Collect()