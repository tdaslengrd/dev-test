﻿################################################

# Below are two sets of powershell script to   #
# migrate dashboard from one account to another#
# account.                                     #
# Make sure to use right source/destination    #
# API and App keys.                            #

################################################


# Export Dashboard:

$api_key = "a84c8530c29aadcb292b2ad804097298"
$app_key = "a97c65db87c49d4e9592a9dd92b1bd05b64fc0bd"
$url_signature = "api/v1/screen"
$url_base = "https://app.datadoghq.com/" 
$url = $url_base + $url_signature + "?api_key=$api_key" + "&" + "application_key=$app_key"
$parameters = (Get-Content appndatabase.json)
write-host $url
Invoke-RestMethod -ContentType 'application/json' -method 'POST' -Uri $url -Body $parameters


# Import DASHBOARD:

$dash_id = "354829"
$api_key = "67352cdfc91b1edf076898bde24007a4"
$app_key = "ac3641d6d7e534c3851512040236e8b38ff42a23"
$url_signature = "api/v1/screen/"
$url_base = "https://app.datadoghq.com/" 
$url = $url_base + $url_signature + $dash_id + "?api_key=$api_key" + "&" + "application_key=$app_key"
Invoke-RestMethod -method 'GET' -Uri $url -Verbose -OutFile C:\dashboard\powershell\appndatabase.jsons
