﻿$wapusers = Get-SCUserRole
$vms = Get-VM | Where {($_.Owner -notlike "") -and ($_.Owner -notlike "*Administrator")}
$vmnetinfos = @()
foreach ($user in $wapusers)
{
    $vmidentifier = $user.Name
    foreach ($vm in $vms)
    {
        if ($vmidentifier -eq $vm.UserRole)
        {
            #write-host ""
            $output = "Scanning " + $vm.Name + " (" + $vm.Owner + ")"
            #write-host $output
            $vminfo = Get-VM -Name $vm.Name
            $networkinfo = $vminfo.VirtualNetworkAdapters
            if ($networkinfo.VMNetwork -notlike "")
            {
                $net = Get-SCVMNetwork -Name $networkinfo.VMNetwork
                $net2 = Get-SCVMNetworkGateway -VMNetwork $net
                $net3 = Get-SCNATConnection -VMNetworkGateway $net2

                #write-host "Getting External IP Addresses for"$vm.Name
                $extips = Get-SCNATRule -VMNetworkGateway $net2 | Select-Object ExternalIPAddress -Unique | ForEach-Object {$_.ExternalIPAddress.IPAddressToString}
                #write-host "Total External IP's Attached -"$extips.Count
                foreach ($extip in $extips)
                {
                    #write-host $extip
                    $stage = $vm.Owner,$vm.Name,$extip
                    $vmnetinfos += , $stage
                }

                #write-host ""
                #write-host "List NAT Rules"
                foreach ($item in $net3)
                {
                    foreach ($rule in $item)
                    {
                        #write-host $rule.Rules
                        #write-host ""
                    }
                }
            }
            else
            {
                #write-host "No network attached!"
            }
        }
    }
    foreach ($vmnetinfo in $vmnetinfos)
    {
        #write-host $vmnetinfo
        $holdarr=@()
        $pNames=@("ClientID","VMName","ExtIP")
        foreach ($row in $vmnetinfos){
            $obj = new-object PSObject
            for ($i=0;$i -lt 3; $i++){
                      $obj | add-member -membertype NoteProperty -name $pNames[$i] -value $row[$i]
              }
           $holdarr+=$obj
           $obj=$null
        }
        $holdarr | export-csv C:\publicmenu.csv -NoTypeInformation
    }
}