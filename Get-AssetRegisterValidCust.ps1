﻿# Get Start Time
$startDTM = (Get-Date)
write-host "Starting query run - $startDTM"

$storagearray = @()

#Go ahead and add the SQL Snapins
add-pssnapin SqlServerCmdletSnapin*

$customerdetails = Invoke-Sqlcmd -Query "Select CustomerNumber, [Machine Name], clientid_sap FROM Machines" -Database assetregisterSQL -serverinstance 40.126.224.18 -Username avnetadminsa -Password wUqpb5W6WfWyQbG
$interfaces = Invoke-Sqlcmd -Query "Select [InterfaceID], [MachineID] FROM Interfaces" -Database assetregisterSQL -serverinstance 40.126.224.18 -Username avnetadminsa -Password wUqpb5W6WfWyQbG
$ips = Invoke-Sqlcmd -Query "Select [IPAddress], [InterfaceID] FROM IPAddressesNew" -Database assetregisterSQL -serverinstance 40.126.224.18 -Username avnetadminsa -Password wUqpb5W6WfWyQbG

$overallcount = 0
$badcount = 0
$goodcount = 0
$allocatedipaddresses = 0

foreach ($item in $customerdetails)
{
    $overallcount++
    if ($item.clientid_sap -like "")
    {
        $badcount++
    }
    else
    {
        #write-host ""
        #write-host $item.CustomerNumber
        #write-host $item.clientid_sap
        #write-host $item.'Machine Name'

        foreach ($interface in $interfaces)
        {
            if ($interface.MachineID -eq $item.CustomerNumber)
            {
                #write-host $interface.InterfaceID
                foreach ($ip in $ips)
                {
                    if ($interface.InterfaceID -eq $ip.InterfaceID)
                    {
                        #write-host $ip.IPAddress
                        $allocatedipaddresses++
                    }
                }
            }
        }
        $combiner = "$item.CustomerNumber,$item.clientid_sap,$item.'Machine Name',$ip.IPAddress"
        $storagearray += , $combiner
        $goodcount++
    }
}

# Get End Time
$endDTM = (Get-Date)

$holdarr=@()
$pNames=@("Customer Number","SAP Contract ID","Device Name","WAN IP Address")
foreach ($row in $storagearray)
{
    $obj = new-object PSObject
    for ($i=0;$i -lt 4; $i++)
    {
        $obj | add-member -membertype NoteProperty -name $pNames[$i] -value $row[$i]
    }
    $holdarr+=$obj
    $obj=$null
}
$holdarr | export-csv C:\araudit.csv -NoTypeInformation

write-host ""
write-host "Total Customers in Database - $overallcount"
write-host "Total Customers with no SAP Contract ID - $badcount"
write-host "Total Customers with a SAP Contract ID - $goodcount"
write-host "Total allocated IP addresses - $allocatedipaddresses"
write-host "Elapsed Time: $(($endDTM-$startDTM).totalseconds) seconds"