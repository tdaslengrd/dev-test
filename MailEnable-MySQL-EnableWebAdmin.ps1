﻿$DataSet = New-Object System.Data.DataSet
$MySQLErrors = ""
$Logfile = "D:\Apps\Logs\$(gc env:computername).log"

Function LogWrite
{
   Param ([string]$logstring)

   Add-Content $Logfile -value $logstring
}

Function QueryMySQL
{
    Param ($Query)

    $MySQLAdminUserName = 'root'
    $MySQLAdminPassword = 'P582kr5u56863zQ'
    $MySQLDatabase = 'mailenable'
    $MySQLHost = 'localhost'
    $ConnectionString = "server=" + $MySQLHost + ";port=3306;uid=" + $MySQLAdminUserName + ";pwd=" + $MySQLAdminPassword + ";database="+$MySQLDatabase

    Try
    {
        [void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data")
        $Connection = New-Object MySql.Data.MySqlClient.MySqlConnection
        $Connection.ConnectionString = $ConnectionString
        $Connection.Open()

        $Command = New-Object MySql.Data.MySqlClient.MySqlCommand($Query, $Connection)
        $DataAdapter = New-Object MySql.Data.MySqlClient.MySqlDataAdapter($Command)
        $RecordCount = $dataAdapter.Fill($DataSet, "data")
        #$DataSet.Tables[0]
    }

    Catch
    {
        $MySQLErrors = "ERROR : Unable to run query : $query `n$Error[0]"
    }

    Finally
    {
        $Connection.Close()
    }
}


#Query MySQL database for the Name of each postoffice which is enabled
LogWrite "Getting all PostOffices which are enabled."
QueryMySQL "Select Name FROM postoffice Where Status = '1';"

#This if statement is a failsafe incase the MySQL command previously has errors, it will exit
if ([String]::IsNullOrEmpty($MySQLErrors))
{
    #Initialise a new DataTable object (dt) and copy the global DataSet object to the new dt object, afterwards clear the original DataSet object.
    $dt = New-Object System.Data.DataSet
    $dt = $DataSet.Copy()
    $DataSet.Clear()

    #Loop through each row in the DataTable (dt)
    foreach ($Row in $dt.Tables[0].Rows)
    { 
        $DomainName = $($Row[0])
        $DomainName = "'" + $DomainName + "'"
        $CheckQuery = "SELECT Query FROM options WHERE Query = " + $DomainName + " AND OptionName = 'WebAdmin-Enabled';"

        QueryMySQL $CheckQuery
        $dt2 = New-Object System.Data.DataSet
        $dt2 = $DataSet.Copy()
        $DataSet.Clear()

        foreach ($Row2 in $dt2.Tables[0].Rows)
        {
            $CheckQueryResult = "'" + $($Row2[1]) + "'" #We need to look at column 1 as the returned results include Name
        }
    
        if ($CheckQueryResult -ne $DomainName) #Check if the returned results is not equal to the domain name were looping through, if not, this means the domain isnt enable for Web Administration
        {
            ###### MailEnable SQL Queries ######
            #### NOTE ####
            #Scope denotes weather its a mailbox or postoffice setting, 1 for postoffice, 2 for mailbox
            #The below settings are the minimum to ensure that Web Administration is setup correctly
            ##############
            ### Enable Web Administraion on PostOffice ###
            $QueryWebAdminEnable = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('1', 'WebAdmin-Enabled', $DomainName, '1');"

            ### Mailbox settings ###
            $QueryMailboxSettingEdit = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('1', 'WebAdmin-CanEditMailboxes', $DomainName, '1');"
            $QueryMailboxSettingMax = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('20', 'WebAdmin-MaxMailboxes', $DomainName, '1');"
            $QueryMailboxSettingSize = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('20000', 'WebAdmin-DefaultMailboxSize', $DomainName, '1');"
            $QueryMailboxSettingSizeEdit = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('1', 'WebAdmin-CanEditMailboxSize', $DomainName, '1');"

            ### Distribution list settings ###
            $QueryListSettingEdit = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('1', 'WebAdmin-CanEditLists', $DomainName, '1');"
            $QueryListSettingMax = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('10', 'WebAdmin-MaxLists', $DomainName, '1');"
            $QueryListSettingMaxMembers = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('10', 'WebAdmin-MaxListMembers', $DomainName, '1');"

            ### Misc settings ###
            $QueryDomainEdit = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('0', 'WebAdmin-CanEditDomains', $DomainName, '1');"
            $QueryDirectoryEdit = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('0', 'WebAdmin-CanEditDirectoryEntries', $DomainName, '1');"
            $QueryWebAdminBranding = "INSERT INTO options (OptionValue, OptionName, Query, Scope) VALUES ('0', 'WebAdmin-Branding', $DomainName, '1');"
            ####################################

            ###### WRITE SECTION NEEDS TESTING ######
            $QueryBuilder = $QueryWebAdminEnable `
                             + $QueryMailboxSettingEdit `
                             + $QueryMailboxSettingMax `
                             + $QueryMailboxSettingSize `
                             + $QueryMailboxSettingSizeEdit `
                             + $QueryListSettingEdit `
                             + $QueryListSettingMax `
                             + $QueryListSettingMaxMembers `
                             + $QueryDomainEdit `
                             + $QueryDirectoryEdit `
                             + $QueryWebAdminBranding

            if ([String]::IsNullOrEmpty($MySQLErrors))
            {
                write-host $QueryBuilder
                #QueryMySQL $QueryWebAdminEnable
            }

            $dt3 = New-Object System.Data.DataSet
            $dt3 = $DataSet.Copy()
            $DataSet.Clear()
            foreach ($Row3 in $dt3.Tables[0].Rows)
            {
                write-host "$($Row3[1])"
                $CheckQueryResult = "'" + $($Row2[1]) + "'"
            }
            #########################################
        }

        $DomainName = ""
        $WriteQuery = ""
        $CheckQueryResult = ""

        Pause
    }
    $dt2.Clear()
}
else
{
    LogWrite $MySQLErrors
}