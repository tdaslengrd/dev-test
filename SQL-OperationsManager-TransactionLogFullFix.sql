USE master
GO
ALTER AVAILABILITY GROUP [OM_12-AG] REMOVE DATABASE OperationsManager;
GO
ALTER DATABASE OperationsManager SET RECOVERY SIMPLE
GO
DECLARE @SQLStatement VARCHAR(2000) 
SET @SQLStatement = '\\1011labfsclap01\SQLBackup\OperationsManager_' + CONVERT(nvarchar(30), GETDATE(), 110) +'.bak'
BACKUP DATABASE OperationsManager TO DISK = @SQLStatement
GO
USE OperationsManager
DBCC shrinkfile('OperationsManager',1) /*/ Shrink down to 1 MB */
GO
USE master
alter database OperationsManager SET RECOVERY FUll
GO
DECLARE @SQLStatement VARCHAR(2000) 
SET @SQLStatement = '\\1011labfsclap01\SQLBackup\OperationsManager_' + CONVERT(nvarchar(30), GETDATE(), 110) +'.bak'
BACKUP DATABASE OperationsManager TO DISK = @SQLStatement
GO
ALTER AVAILABILITY GROUP [OM_12-AG] ADD DATABASE OperationsManager;
GO