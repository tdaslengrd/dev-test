﻿Try
{
    # Filename of the export
    $filename = “RDS-CAL-Report.csv”

    # Import RDS PowerShell Module
    $i = Get-Module -List -Name "remotedesktopservices"
    if(!$i)
    {
        write-host "$env:ComputerName does not have RDS/Terminal Services installed!"
        exit
    }
    import-module remotedesktopservices

    # Open RDS Location
    Set-Location -path rds:

    # Remove previous reports (Optional)
    remove-item RDS:\LicenseServer\IssuedLicenses\PerUserLicenseReports\* -Recurse

    # Create new RDS report
    Invoke-WmiMethod -Class Win32_TSLicenseReport -Name GenerateReportEx

    # Name is automatically generated
    $NewReportName = Get-ChildItem RDS:\LicenseServer\IssuedLicenses\PerUserLicenseReports -name

    # Get issued licenses
    $IssuedLicenseCount = get-item RDS:\LicenseServer\IssuedLicenses\PerUserLicenseReports\$NewReportName\Win8\IssuedCount

    # Count issued licenses
    $IssuedLicenseCountValue = $IssuedLicenseCount.CurrentValue

    # Get installed licenses
    $InstalledLicenseCount = get-item RDS:\LicenseServer\IssuedLicenses\PerUserLicenseReports\$NewReportName\Win8\InstalledCount

    # Count installed licenses
    $InstalledLicenseCountValue = $InstalledLicenseCount.CurrentValue

    write-host $InstalledLicenseCount

    # Installed – Issued
    $Available = $InstalledLicenseCount.CurrentValue – $IssuedLicenseCount.CurrentValue

    # Show percentage available
    $AvailablePercent = ($Available /$InstalledLicenseCount.CurrentValue)*100
    $AvailablePercent = “{0:N0}” -f $AvailablePercent

    # Display info
    Write-host “Installed: $InstalledLicenseCountValue”
    Write-host “Issued: $IssuedLicenseCountValue”
    Write-host “Available: $Available [ $AvailablePercent % ]”

    # Add the information into an Array
    [System.Collections.ArrayList]$collection = New-Object System.Collections.ArrayList($null)
    $obj = @{
                Installed = $InstalledLicenseCountValue
                Available = $Available
                AvailablePercent = $AvailablePercent
                Issued = $IssuedLicenseCountValue
                Date = get-date
            }
}
Catch
{
    write-host "No RDS role/feature found!"
}


