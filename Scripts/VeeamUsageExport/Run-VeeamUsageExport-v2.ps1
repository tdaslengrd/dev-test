﻿# Start Time command
$sw = [Diagnostics.Stopwatch]::StartNew()

#SQL Connection Info
$SQLServer = "10.100.0.249"
$SQLDBName = "VeeamUsageData"
$uid ="backupuser"
$pwd = "Passw0rd"
$connectionString = "Data Source=$SQLServer;Initial Catalog=$SQLDBName; Integrated Security = False; User ID = $uid; Password = $pwd;"
$TableName = get-date -UFormat %B_%Y_%V
$createTable = @"
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[$TableName]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[$TableName]
	(
		[id] [int] IDENTITY(1,1) NOT NULL,
        [veeamserver] [varchar](50) NULL,
        [jobtype] [varchar](50) NULL,
		[jobname] [varchar](250) NULL,
		[totalbackupsize] [bigint] NULL,
		[totaldatasize] [bigint] NULL,
		[vmcount] [int] NULL,
		[vms] [varchar](250) NULL,
		[datetime] [datetime] NULL,
	)
END;
"@

$Date = Get-Date -format dd-MM-yyyy
$dt = get-date -Format "yyyy-MM-dd"
$BasePath = "C:\etc\Scripts\VeeamUsageExport\Logs\$Date.txt"

$VeeamServer = $env:computername
$VeeamUserName = "admin9431"
$VeeamPassword = "gHfcXX95p"

if ((Get-PSSnapin -Name VeeamPSSnapIn -ErrorAction SilentlyContinue) -eq $null)
{
    Add-PsSnapin -Name VeeamPSSnapIn
}

function Invoke-SQLQuery
{
    param([string]$connectionString, [string]$sqlCommand)

    $connection = new-object system.data.SqlClient.SQLConnection($connectionString)
    $command = new-object system.data.sqlclient.sqlcommand($sqlCommand,$connection)
    
    $connection.Open()
    $command.ExecuteNonQuery()
    $connection.Close()
}

function Write-Log 
{ 
    [CmdletBinding()] 
    Param 
    ( 
        [Parameter(Mandatory=$true, 
                   ValueFromPipelineByPropertyName=$true)] 
        [ValidateNotNullOrEmpty()] 
        [Alias("LogContent")] 
        [string]$Message, 
 
        [Parameter(Mandatory=$false)] 
        [Alias('LogPath')] 
        [string]$Path='C:\Logs\PowerShellLog.log', 
         
        [Parameter(Mandatory=$false)] 
        [ValidateSet("Error","Warn","Info")] 
        [string]$Level="Info", 
         
        [Parameter(Mandatory=$false)] 
        [switch]$NoClobber 
    ) 
 
    Begin 
    { 
        # Set VerbosePreference to Continue so that verbose messages are displayed. 
        $VerbosePreference = 'Continue' 
    } 
    Process 
    { 
         
        # If the file already exists and NoClobber was specified, do not write to the log. 
        if ((Test-Path $Path) -AND $NoClobber) { 
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name." 
            Return 
            } 
 
        # If attempting to write to a log file in a folder/path that doesn't exist create the file including the path. 
        elseif (!(Test-Path $Path)) { 
            Write-Verbose "Creating $Path." 
            $NewLogFile = New-Item $Path -Force -ItemType File 
            } 
 
        else { 
            # Nothing to see here yet. 
            } 
 
        # Format Date for our Log File 
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss" 
 
        # Write message to error, warning, or verbose pipeline and specify $LevelText 
        switch ($Level) { 
            'Error' { 
                Write-Error $Message 
                $LevelText = 'ERROR:' 
                } 
            'Warn' { 
                Write-Warning $Message 
                $LevelText = 'WARNING:' 
                } 
            'Info' { 
                Write-Verbose $Message 
                $LevelText = 'INFO:' 
                } 
            } 
         
        # Write log entry to $Path 
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append 
    } 
    End 
    { 
    } 
}

Try
{
    Write-Log -Message "Starting Veeam Backup usage export for $VeeamServer" -Path $BasePath -Level Info
    Connect-VBRServer -User $VeeamUserName -Password $VeeamPassword -Server $VeeamServer

    #Lets create a database table for the billing day, incase this is rerun we will also check if the table already exists
    #Invoke-Sqlcmd -Username $uid -Password $pwd -ServerInstance $SQLServer -Database $SQLDBName -Query $createTable
    Invoke-SQLQuery -connectionString $connectionString -sqlCommand $createTable | Out-Null

	Write-Log -Message "Loading backup jobs from Veeam" -Path $BasePath -Level Info
    $backupJobs = Get-VBRBackup

	foreach ($job in $backupJobs)
	{
		# get all restore points inside this backup job
		$restorePoints = $job.GetAllStorages() | sort CreationTime -descending

		$jobBackupSize = 0;
		$jobDataSize = 0;

		$jobName = ($job | Select -ExpandProperty JobName);
        $jobType = ($job | Select -ExpandProperty JobType);

		Write-Log -Message "Processing backup job: $jobName" -Path $BasePath -Level Info

		# get list of VMs associated with this backup job
		$vmList = ($job | Select @{n="vm";e={$_.GetObjectOibsAll() | %{@($_.name,"")}}} | Select -ExpandProperty vm);
		$amountVMs = 0;
		$vms = ""
		foreach($vmName in $vmList)
		{
			if([string]::IsNullOrEmpty($vmName)) {
				continue
			}
			$vms += "$vmName,"
			$amountVMs = $amountVMs + 1
		}

		# cut last ,
		if(![string]::IsNullOrEmpty($vmName)) {
			$vms = $vms.Substring(0, $vms.Length - 1);
		}

		# go through restore points and add up the backup and data sizes
		foreach ($point in $restorePoints)
		{
			$jobBackupSize += [long]($point | Select-Object -ExpandProperty stats | Select -ExpandProperty BackupSize);
			$jobDataSize += [long]($point | Select-Object -ExpandProperty stats | Select -ExpandProperty DataSize);
		}

		# convert to GB
		[Int64]$jobBackupSize = [math]::Round(($jobBackupSize / 1024 / 1024 / 1024), 2);
		[Int64]$jobDataSize = [math]::Round(($jobDataSize / 1024 / 1024 / 1024), 2);

        Write-Log -Message "Writing out data to SQL" -Path $BasePath -Level Info
        $write2SQL = "INSERT INTO [$SQLDBName].[dbo].[$TableName] (veeamserver, jobtype, jobname, totalbackupsize, totaldatasize, vmcount, vms, datetime) VALUES ('$VeeamServer', '$jobType', '$jobName', '$jobBackupSize', '$jobDataSize', '$amountVMs', '$vms', '$dt');";
        Invoke-SQLQuery -connectionString $connectionString -sqlCommand $write2SQL | Out-Null

        Write-Log -Message "Finished processing job $jobName" -Path $BasePath -Level Info
	}

    Write-Log -Message "Loading replication jobs from Veeam" -Path $BasePath -Level Info
    $replicaJobs = Get-VBRReplica

	foreach ($job in $replicaJobs)
	{
		# get all restore points inside this backup job
		$restorePoints = $job.GetAllStorages() | sort CreationTime -descending

		$jobBackupSize = 0;
		$jobDataSize = 0;

		$jobName = ($job | Select -ExpandProperty JobName);
        $jobType = ($job | Select -ExpandProperty JobType);

		Write-Log -Message "Processing backup job: $jobName" -Path $BasePath -Level Info

		# get list of VMs associated with this backup job
		$vmList = ($job | Select @{n="vm";e={$_.GetObjectOibsAll() | %{@($_.name,"")}}} | Select -ExpandProperty vm);
		$amountVMs = 0;
		$vms = ""
		foreach($vmName in $vmList)
		{
			if([string]::IsNullOrEmpty($vmName)) {
				continue
			}
			$vms += "$vmName,"
			$amountVMs = $amountVMs + 1
		}

		# cut last ,
		if(![string]::IsNullOrEmpty($vmName)) {
			$vms = $vms.Substring(0, $vms.Length - 1);
		}

		# go through restore points and add up the backup and data sizes
		foreach ($point in $restorePoints)
		{
			$jobBackupSize += [long]($point | Select-Object -ExpandProperty stats | Select -ExpandProperty BackupSize);
			$jobDataSize += [long]($point | Select-Object -ExpandProperty stats | Select -ExpandProperty DataSize);
		}

		# convert to GB
		[Int64]$jobBackupSize = [math]::Round(($jobBackupSize / 1024 / 1024 / 1024), 2);
		[Int64]$jobDataSize = [math]::Round(($jobDataSize / 1024 / 1024 / 1024), 2);

        Write-Log -Message "Writing out data to SQL" -Path $BasePath -Level Info
        $write2SQL = "INSERT INTO [$SQLDBName].[dbo].[$TableName] (veeamserver, jobtype, jobname, totalbackupsize, totaldatasize, vmcount, vms, datetime) VALUES ('$VeeamServer', '$jobType', '$jobName', '$jobBackupSize', '$jobDataSize', '$amountVMs', '$vms', '$dt');";
        Invoke-SQLQuery -connectionString $connectionString -sqlCommand $write2SQL | Out-Null

        Write-Log -Message "Finished processing job $jobName" -Path $BasePath -Level Info
	}
}
Catch
{
    Write-Log -Message "Error - $_.Exception.Message" -Path $BasePath -Level Error
    write-host $_.Exception | format-list -force
}
Finally
{
    Disconnect-VBRServer

    # Stop Time
    $sw.Stop()
    # Store Elapsed time into desired vars
    $min = $sw.Elapsed.Minutes
    # Display timer statistics to host
    Write-Log -Message "Total elapsed time $min minutes." -Path $BasePath -Level Info

    Write-Log -Message "Job Finished." -Path $BasePath -Level Info
}