﻿param(
	[bool]$isCheckRun # check run sends errors but doesn't update database
)

function execute_sql([string]$sql, [string]$connectionstring, [int]$type)
{
	$connection = New-Object System.Data.SqlClient.SqlConnection
	$connection.ConnectionString = $connectionstring
	$connection.Open()
	$command = $connection.CreateCommand()
	$command.CommandText = $sql
	if ($type -eq $sqlselect) {
		$result = $command.ExecuteScalar()
		$connection.Close()
		return $result
	}
	if ($type -eq $sqledit) {
		$command.executenonquery()
	}
	$connection.Close()
}
Add-PSSnapin *veeam*
$starttime = get-date
$datetime = get-date -format "yyyyMMdd HH:mm:ss"
$sqlselect = 0
$sqledit = 1
$servers=@()
$aregconnectionString = "Server=192.168.125.2;Database=assetregisterSQL;Uid=sa;Pwd=FT&I9Hmj;"
$veeamconnectionString = "Server=10.100.0.74;Database=VeeamBackup;Uid=sa;Pwd=FT&I9Hmj;"

###### get next job id number
$sql = "select max(jobid) from BackupDataJob"
$jobid = (execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqlselect) + 1
####

#$jobs = get-vbrjob | where {$_.JobType -eq "Replica"} | sort-object name

###### GET VEEAM JOBS
# get a list of all veeam backup jobs
#$jobs = get-vbrjob -name 4928abvirt | where {$_.JobType -eq "Replica"} 
$jobs = get-vbrjob | where {$_.JobType -eq "Backup"} | sort-object name
foreach ($job in $jobs)
{
	# can only get the ACTUAL target directory from the SQL DB as far as I know
	write-host "veeam "$job.name
	if ($job.CanRunByScheduler()) {
		$sql = "select dir_path from [Backup.Model.Backups] where job_id = '" + $job.id + "'"
		$targetdir = execute_sql -sql $sql -connectionstring $veeamconnectionString -type $sqlselect
		
		if ($targetdir -ne $null) {
			$targetdir = $targetdir.replace(':','$')
			$targethost = $job.GetTargetHost().name
			
			#check if it is this server then the hostname is not used
			if ($targethost -eq "This server") {
				$targethost = ""
			} else {
				$targethost = "\\" + $targethost + "\"
			}
			$uncpath =  $targethost + $targetdir + "\"
            $rawpath = $targetdir
			####
			
			$object = New-Object PSObject -Property @{
				hostname	= $job.name
				uncpath		= $uncpath
                rawpath = $rawpath
			}
			$servers += $object
		}
	}
}

###### GET SP/ACRONIS/ULTRABAC JOBS
#$rootfolders = get-content .\rootfolders.txt
#foreach ($rootfolder in $rootfolders)
#{
#	$folders = get-childitem -path $rootfolder -directory
#	foreach ($folder in $folders)
#	{
#		$uncpath = "$rootfolder\$($folder.name)\"
#		write-host "sp "$folder.name
#		$object = New-Object PSObject -Property @{
#			hostname	= $folder.name
#		}
#		$servers += $object
#	}
#}

###### CALCULATE DATA AND ADD TO DB
$errors = "Please fix the following errors before Fridays bill run. Especially if it's the last week of the month!"
foreach ($server in $servers)
{
    if($server.uncpath -like "\\1000VeeamHB02*")
    {
        write-host "This isnt a SMB share!"
        write-host $server | fl
        pause
        # check we have files first, or we'll get an error
	    if ((get-childitem $server.rawpath -file).count) {	
		    $size = get-childitem $server.rawpath | measure-object -property length -sum
		    $sizeGB = [math]::Round($size.sum / 1GB,2)

		    # check db for server and get customer number
		    $sql = "SELECT CustomerNumber FROM Machines WHERE [Machine Name] = '" + $server.hostname + "'"
		    $clientid = execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqlselect
		    if (!$clientid) {
			    $clientid = "HOST NOT FOUND"
			    $errors += "`n`r$($server.hostname) HOST NOT FOUND"
		    }
		
		    # check db for server and get contract number
		    $sql = "SELECT contract_backup_sap FROM Machines WHERE [Machine Name] = '" + $server.hostname + "'"		
		    $contract_backup = execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqlselect
		    # host not found in db -or db has a null value
		    if (!$contract_backup -or [System.DBNull]::Value.Equals($contract_backup)) {
			    $contract_backup = "UNKNOWN CONTRACT NUMBER"
			    $errors += "`n`r$($server.hostname) UNKNOWN CONTRACT NUMBER"
		    }
		


		    $sql = "insert into backupdatausage (jobid,machinename,clientid,GB,path,contract_backup_sap) values ($jobid,'$($server.hostname)','$clientid',$sizeGB,'$($server.uncpath)','$contract_backup')"
		    write-host $sql
		    # write to database
		    if (!$isCheckRun)
		    {
			    execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqledit
		    }
		    ####
	    }
    }
    else
    {
        write-host "This is a SMB share!"
        write-host $server | fl
        pause
        # check we have files first, or we'll get an error
	    if ((get-childitem $server.uncpath -file).count) {	
		    $size = get-childitem $server.uncpath | measure-object -property length -sum
		    $sizeGB = [math]::Round($size.sum / 1GB,2)
		
		    # check db for server and get customer number
		    $sql = "SELECT CustomerNumber FROM Machines WHERE [Machine Name] = '" + $server.hostname + "'"
		    $clientid = execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqlselect
		    if (!$clientid) {
			    $clientid = "HOST NOT FOUND"
			    $errors += "`n`r$($server.hostname) HOST NOT FOUND"
		    }
		
		    # check db for server and get contract number
		    $sql = "SELECT contract_backup_sap FROM Machines WHERE [Machine Name] = '" + $server.hostname + "'"		
		    $contract_backup = execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqlselect
		    # host not found in db -or db has a null value
		    if (!$contract_backup -or [System.DBNull]::Value.Equals($contract_backup)) {
			    $contract_backup = "UNKNOWN CONTRACT NUMBER"
			    $errors += "`n`r$($server.hostname) UNKNOWN CONTRACT NUMBER"
		    }
		


		    $sql = "insert into backupdatausage (jobid,machinename,clientid,GB,path,contract_backup_sap) values ($jobid,'$($server.hostname)','$clientid',$sizeGB,'$($server.uncpath)','$contract_backup')"
		    write-host $sql
		    # write to database
		    if (!$isCheckRun)
		    {
			    execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqledit
		    }
		    ####
	    }
    }
}

if ($isCheckRun)
#### Send errors list as this is a check run
{
     $smtpServer = "internalmail.ico.com.au"
     $msg = new-object Net.Mail.MailMessage
     $smtp = new-object Net.Mail.SmtpClient($smtpServer)

     #Email structure
     $msg.From = "backup_data@ico.com.au"
     #$msg.ReplyTo = "replyto@xxxx.com"
     $msg.To.Add("jet.gopaldas@techdata.com")
	 $msg.To.Add("aus-ats-services-operations@techdata.com")
     $msg.subject = "Backup billing errors"
     $msg.body = $errors

     #Sending email
     $smtp.Send($msg)

	#write-host "Email sent" 
}

# end timer
$endtime = get-date
$run_mins = New-TimeSpan -start $starttime -end (get-date)

# add backupdatajob details
if (!$isCheckRun)
{
	$sql = "insert into [BackupDataJob] (jobid, date, run_mins, sap) values ($jobid,'$datetime',$($run_mins.TotalMinutes),1)"
	execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqledit
}