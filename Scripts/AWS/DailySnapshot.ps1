############## C O N F I G ##############
."C:\etc\Scripts\AWS\AWSConfig.ps1"

############## F U N C T I O N S ##############
."C:\etc\Scripts\AWS\AWSUtilities.ps1"

#Environment
$ENVIRONMENT_NAME = "Innovit US AWS Environment"
$ENVIRONMENT_TYPE = "Production"
$BACKUP_TYPE = "Daily"
$backupTag = "DailyBackup" #Make sure the value of this Tag is 'Yes', without the quotes, on the instances you want backed up

############## M A I N ##############

try
{
    $start = Get-Date
    WriteToLogAndEmail "$ENVIRONMENT_NAME $ENVIRONMENT_TYPE $BACKUP_TYPE Backup Starting" -excludeTimeStamp $true
    
    $stagingInstanceIDs= GetBackedUpInstances $backupTag

    CreateSnapshotsForInstances $stagingInstanceIDs

    CleanupDailySnapshots

    WriteToLogAndEmail "$ENVIRONMENT_NAME $ENVIRONMENT_TYPE $BACK_UPTYPE Backup Complete" -excludeTimeStamp $true   
    
    $end = Get-Date
    $timespan = New-TimeSpan $start $end
    $hours=$timespan.Hours
    $minutes=$timespan.Minutes    
    WriteToEmail "Backup took $hours hour(s) and $minutes minute(s)"
    
    SendStatusEmail -successString "SUCCESS"
}
catch
{
    SendStatusEmail -successString "FAILED"
}
