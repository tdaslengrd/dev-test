﻿#Access Keys
$accessKeyID="AKIAIYVCAVFHFNWABDNA"
$secretAccessKey="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"
$accountID = "358884484514"

# Create an AMI from a running instance by using the instance's name tag, tag the resulting AMI and all snapshots with a meaningful tag

Set-AWSCredentials -AccessKey $accessKeyID -SecretKey $secretAccessKey -StoreAs myAWScredentials
Set-AWSCredentials -StoredCredentials myAWScredentials

$region = "ap-southeast-2"

$array = @("northvirginia01.innovit.com") # Name of servers to be restarted
 
foreach ($instanceName in $array) {
 
    $instanceID = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances #Get instance ID
 
    $longTime =  Get-Date -Format "yyyy-MM-dd_HH-mm-ss" # Get current time into a string
    $tagDesc = "Created by " + $MyInvocation.MyCommand.Name + " on " + $longTime # Make a nice string for the Description tag
    $amiName = $instanceName + " AMI " + $longTime # Make a name for the AMI
 
    $amiID = New-EC2Image -InstanceId $instanceID.InstanceId -Description $tagDesc -Name $amiName -NoReboot:$true # Create the AMI, rebooting the instance in the process
 
    Start-Sleep -Seconds 60 # For some reason, it can take some time for subsequent calls to Get-EC2Image to return all properties, especially for snapshots. So we wait
 
    $shortTime = Get-Date -Format "yyyy-MM-dd" # Shorter date for the name tag
    $tagName = $instanceName + " AMI " + $shortTime # Sting for use with the name TAG -- as opposed to the AMI name, which is something else and set in New-EC2Image
 
    New-EC2Tag -Resources $amiID -Tags @( @{ Key = "Name" ; Value = $tagName}, @{ Key = "Description"; Value = $tagDesc } ) # Add tags to new AMI
     
    $amiProperties = Get-EC2Image -ImageIds $amiID # Get Amazon.EC2.Model.Image
  
    $amiBlockDeviceMapping = $amiProperties.BlockDeviceMapping # Get Amazon.Ec2.Model.BlockDeviceMapping
  
    $amiBlockDeviceMapping.ebs | `
    ForEach-Object -Process {New-EC2Tag -Resources $_.SnapshotID -Tags @{ Key = "Name" ; Value = $amiName} }# Add tags to snapshots associated with the AMI using Amazon.EC2.Model.EbsBlockDevice 
    
    "Created AMI" + " " + $amiID + " " + $amiName | Out-File -FilePath "C:\etc\Scripts\AWS\Logs-AMI\$instanceName-AMI.log" -Append
 
   } # End foreach

Clear-AWSCredentials