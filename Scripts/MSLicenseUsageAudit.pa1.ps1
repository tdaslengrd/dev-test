﻿Function SQLbuild2name {
    Param ($build)    
    Switch -wildcard ($build) {
        "8.0*"    { $name = "SQL Server 2000"}
        "9.0*"    { $name = "SQL Server 2005"}
        "10.0*"    { $name = "SQL Server 2008"}
        "10.5*"    { $name = "SQL Server 2008R2"}
        "11.**"    { $name = "SQL Server 2012"}
        Default    { $name = "Unknown version"}
    } 
    $name
}

Function Get-SQLserverKey {
    ## function to retrieve the Windows Product Key from any PC as well as the SQL Server Product Key
    ## by Jakob Bindslet (jakob@bindslet.dk), last updated 2012-07-14
    Param ($targets = $env:computername)
    $hklm = 2147483650
    $regPath2005 = "SOFTWARE\Microsoft\Microsoft SQL Server\90\ProductID"
    $regPath2005version = "SOFTWARE\Microsoft\Microsoft SQL Server\90\Tools\ClientSetup\CurrentVersion"
    $regPath2008 = "SOFTWARE\Microsoft\Microsoft SQL Server\100\Tools\Setup"
    $regPath2012 = "SOFTWARE\Microsoft\Microsoft SQL Server\110\Tools\Setup"
    $regValue2005 = "DigitalProductId77591"
    $regValue2008 = "DigitalProductId"
    $regValue2012 = "DigitalProductId"
    $regValueVersion2005 = "CurrentVersion"
    $regValueVersion2008 = "Version"
    $regValueVersion2012 = "Version"
    $SQLinstalled = $false
    Foreach ($target in $targets) {
        $productKey = $null
        $win32os = $null
        $sqlversion = $null
        $wmi = [WMIClass]"\\$target\root\default:stdRegProv"
        $data2005 = $wmi.GetBinaryValue($hklm,$regPath2005,$regValue2005)
        $data2008 = $wmi.GetBinaryValue($hklm,$regPath2008,$regValue2008)
        $data2012 = $wmi.GetBinaryValue($hklm,$regPath2012,$regValue2012)
        $productKey = ""
        if ($data2005.uValue.length -ge 1) { 
            $binArray2005 = ($data2005.uValue)[52..66]
            $binArray = $binArray2005
            $sqlversion = $wmi.GetStringValue($hklm,$regPath2005version,$regValueVersion2005).sValue
            $SQLinstalled = $true
        }
        if ($data2008.uValue.length -ge 1) { 
            $binArray2008 = ($data2008.uValue)[52..66]
            $binArray = $binArray2008
            $sqlversion = $wmi.GetStringValue($hklm,$regPath2008,$regValueVersion2008).sValue
            $SQLinstalled = $true
        }
        if ($data2012.uValue.length -ge 1) { 
            $binArray2012 = ($data2012.uValue)
            $binArray = $binArray2012
            $sqlversion = $wmi.GetStringValue($hklm,$regPath2012,$regValueVersion2012).sValue
            $SQLinstalled = $true
           }

        if ($SQLinstalled) {
            ## decrypt base24 encoded binary data
            $charsArray = "B","C","D","F","G","H","J","K","M","P","Q","R","T","V","W","X","Y","2","3","4","6","7","8","9"
            For ($i = 24; $i -ge 0; $i--) {
                $k = 0
                For ($j = 14; $j -ge 0; $j--) {
                    $k = $k * 256 -bxor $binArray[$j]
                    $binArray[$j] = [math]::truncate($k / 24)
                    $k = $k % 24
                }
                $productKey = $charsArray[$k] + $productKey
                If (($i % 5 -eq 0) -and ($i -ne 0)) {
                    $productKey = "-" + $productKey
                }
            }
        } else {
            $productKey = "" #SQL not installed
        }

        if($productKey -ne "")
        {
            $InfoRecord = New-Object -TypeName PSObject -Property @{
                                                                    'sqlinstalled' = "Yes";
                                                                    'productkey' = $productKey;
                                                                    'productversion' = "$(SQLbuild2name $sqlversion) ($sqlversion)";
                                                                    }
        }
        else
        {
            $InfoRecord = New-Object -TypeName PSObject -Property @{
                                                                    'sqlinstalled' = "No";
                                                                    'productkey' = "";
                                                                    'productversion' = "";
                                                                    }
        }
        #Write-Output $InfoRecord
        $InfoRecord

        
    }
}

Function Get-CPUInfo {
    $ComputerName = "localhost"

    #Get processors information            
    $CPU=Get-WmiObject -ComputerName $ComputerName -class Win32_Processor
    #Get Computer model information
    $OS_Info=Get-WmiObject -ComputerName $ComputerName -class Win32_ComputerSystem
            
    #Reset number of cores and use count for the CPUs counting
    $CPUs = 0
    $Cores = 0
           
    foreach($Processor in $CPU)
    {
        $CPUs = $CPUs+1   
           
        #count the total number of cores         
        $Cores = $Cores+$Processor.NumberOfCores
    } 
           
    $InfoRecord = New-Object -TypeName PSObject -Property @{
                                                                Server = $ComputerName;
                                                                'CPUs' = $CPUs;
                                                                'Cores' = $Cores;
                                                                'TotalCores' = $Cores*$CPUs;
                                                            }
    #Write-Output $InfoRecord
    $InfoRecord
}

Function Get-WindowsKey {
    ## function to retrieve the Windows Product Key from any PC
    ## by Jakob Bindslet (jakob@bindslet.dk)
    param ($targets = ".")
    $hklm = 2147483650
    $regPath = "Software\Microsoft\Windows NT\CurrentVersion"
    $regValue = "DigitalProductId"
    Foreach ($target in $targets) {
        $productKey = $null
        $win32os = $null
        $wmi = [WMIClass]"\\$target\root\default:stdRegProv"
        $data = $wmi.GetBinaryValue($hklm,$regPath,$regValue)
        $binArray = ($data.uValue)[52..66]
        $charsArray = "B","C","D","F","G","H","J","K","M","P","Q","R","T","V","W","X","Y","2","3","4","6","7","8","9"
        ## decrypt base24 encoded binary data
        For ($i = 24; $i -ge 0; $i--) {
            $k = 0
            For ($j = 14; $j -ge 0; $j--) {
            $k = $k * 256 -bxor $binArray[$j]
            $binArray[$j] = [math]::truncate($k / 24)
            $k = $k % 24
        }
        $productKey = $charsArray[$k] + $productKey
        If (($i % 5 -eq 0) -and ($i -ne 0)) {
            $productKey = "-" + $productKey
        }
    }
    $win32os = Get-WmiObject Win32_OperatingSystem -computer $target
    #$obj = New-Object Object
    #$obj | Add-Member Noteproperty Computer -value $target
    #$obj | Add-Member Noteproperty Caption -value $win32os.Caption
    #$obj | Add-Member Noteproperty CSDVersion -value $win32os.CSDVersion
    #$obj | Add-Member Noteproperty OSArch -value $win32os.OSArchitecture
    #$obj | Add-Member Noteproperty BuildNumber -value $win32os.BuildNumber
    #$obj | Add-Member Noteproperty RegisteredTo -value $win32os.RegisteredUser
    #$obj | Add-Member Noteproperty ProductID -value $win32os.SerialNumber
    #$obj | Add-Member Noteproperty ProductKey -value $productkey
    #$obj

    $InfoRecord = New-Object -TypeName PSObject -Property @{
                                                            ProductID = $win32os.SerialNumber;
                                                            ProductKey = $productkey;
                                                            }
    $InfoRecord
    }
}

Function Get-RDSLicenses {
    # Filename of the export
    $filename = “RDS-CAL-Report.csv”

    # Import RDS PowerShell Module
    $i = Get-Module -List -Name "remotedesktopservices"
    if(!$i)
    {
        #write-host "$env:ComputerName does not have RDS/Terminal Services installed!"
        # Add the information into an Array
        [System.Collections.ArrayList]$collection = New-Object System.Collections.ArrayList($null)
        $InfoRecord = @{
                            Installed = ""
                            Available = ""
                            AvailablePercent = ""
                            Issued = ""
                            Date = get-date
                            RDSInstalled = "No"
                        }
        $InfoRecord
    }
    else
    {
        import-module remotedesktopservices

        # Open RDS Location
        Set-Location -path rds:

        # Remove previous reports (Optional)
        remove-item RDS:\LicenseServer\IssuedLicenses\PerUserLicenseReports\* -Recurse

        # Create new RDS report
        Invoke-WmiMethod -Class Win32_TSLicenseReport -Name GenerateReportEx | Out-Null

        # Name is automatically generated
        $NewReportName = Get-ChildItem RDS:\LicenseServer\IssuedLicenses\PerUserLicenseReports -name

        # Get issued licenses
        $IssuedLicenseCount = get-item RDS:\LicenseServer\IssuedLicenses\PerUserLicenseReports\$NewReportName\Win8\IssuedCount

        # Count issued licenses
        $IssuedLicenseCountValue = $IssuedLicenseCount.CurrentValue

        # Get installed licenses
        $InstalledLicenseCount = get-item RDS:\LicenseServer\IssuedLicenses\PerUserLicenseReports\$NewReportName\Win8\InstalledCount

        # Count installed licenses
        $InstalledLicenseCountValue = $InstalledLicenseCount.CurrentValue

        # Installed – Issued
        $Available = $InstalledLicenseCount.CurrentValue – $IssuedLicenseCount.CurrentValue

        # Display info
        #Write-Host "RDS License Information"
        #Write-host “Installed: $InstalledLicenseCountValue”
        #Write-host “Issued: $IssuedLicenseCountValue”
        #Write-host “Available: $Available [ $AvailablePercent % ]”

        # Add the information into an Array
        [System.Collections.ArrayList]$collection = New-Object System.Collections.ArrayList($null)
        $InfoRecord = @{
                            Installed = $InstalledLicenseCountValue
                            Available = $Available
                            Issued = $IssuedLicenseCountValue
                            Date = get-date
                            RDSInstalled = "Yes"
                        }
        $InfoRecord
    }
}

Function Get-ExchangeLicense {
    # Import Exchange PowerShell Module
    Add-PSSnapin *exchange* -ErrorAction SilentlyContinue
    $i = Get-PSSnapin -Name "*exchange*" -ErrorAction SilentlyContinue
    if(!$i)
    {
        # Add the information into an Array
        [System.Collections.ArrayList]$collection = New-Object System.Collections.ArrayList($null)
        $InfoRecord = @{
                            MailboxCount = ""
                            Edition = ""
                            ExInstalled = "No"
                       }
        $InfoRecord
    }
    else
    {
        #write-host (Get-Mailbox -resultsize unlimited).Count
        #write-host (Get-ExchangeServer).Edition

        [System.Collections.ArrayList]$collection = New-Object System.Collections.ArrayList($null)
        $InfoRecord = @{
                            MailboxCount = (Get-Mailbox -resultsize unlimited).Count
                            Edition = (Get-ExchangeServer).Edition
                            ExInstalled = "Yes"
                       }
        $InfoRecord
    }
}

Function Get-LicenseReport {
    $CPUInfo = Get-CPUInfo

    $OSInfo = Get-WindowsKey

    $RDSInfo = Get-RDSLicenses

    $SQLInfo = Get-SQLserverKey

    $ExInfo = Get-ExchangeLicense

    $win32os = Get-WmiObject Win32_OperatingSystem -computer $env:ComputerName

    $obj = New-Object Object
    $obj | Add-Member Noteproperty Computer -value $env:ComputerName
    $obj | Add-Member Noteproperty Caption -value $win32os.Caption
    $obj | Add-Member Noteproperty Version -value $win32os.Version
    #$obj | Add-Member Noteproperty OSArch -value $win32os.OSArchitecture
    $obj | Add-Member Noteproperty CPUs -value $CPUInfo.CPUs
    $obj | Add-Member Noteproperty Cores -value $CPUInfo.Cores
    $obj | Add-Member Noteproperty TotalCores -value $CPUInfo.TotalCores
    #$obj | Add-Member Noteproperty BuildNumber -value $win32os.BuildNumber
    #$obj | Add-Member Noteproperty RegisteredTo -value $win32os.RegisteredUser
    #$obj | Add-Member Noteproperty "ProductID (Windows)" -value $win32os.SerialNumber
    $obj | Add-Member Noteproperty "ProductKey (Windows)" -value $OSInfo.ProductKey
    $obj | Add-Member Noteproperty "SQL Installed" -value $SQLInfo.sqlinstalled
    $obj | Add-Member Noteproperty "SQL Version" -value $SQLInfo.productversion
    $obj | Add-Member Noteproperty "ProductKey (SQL Server)" -value $SQLInfo.productkey
    $obj | Add-Member Noteproperty "RDS Installed" -value $RDSInfo.RDSInstalled
    $obj | Add-Member Noteproperty "Installed RDS Licenses" -value $RDSInfo.Installed
    $obj | Add-Member Noteproperty "Used RDS Licenses" -value $RDSInfo.Available
    $obj | Add-Member Noteproperty "Available RDS Licenses" -value $RDSInfo.Issued
    $obj | Add-Member Noteproperty "Exchange Installed" -value $ExInfo.ExInstalled
    $obj | Add-Member Noteproperty "Provisioned Mailbox's" -value $ExInfo.MailboxCount
    $obj | Add-Member Noteproperty "Exchange Edition" -value $ExInfo.Edition
    $obj
}

Get-LicenseReport