﻿Login-AzureRmAccount

$adsObjects = New-Object System.Collections.ArrayList

#Get the list of recovery services vaults in the subscription
$VaultObjs = Get-AzureRmRecoveryServicesVault
foreach ($VaultObj in $VaultObjs) 
{
    write-host "Processing vault $($VaultObj.Name)"

    #Get and import vault settings file
    $VaultFileLocation = Get-AzureRmRecoveryServicesVaultSettingsFile -SiteRecovery -Vault $VaultObj
    Import-AzureRmRecoveryServicesAsrVaultSettingsFile -Path $VaultFileLocation.FilePath | Out-Null
 
    #Get the list of fabrics in a vault
    $Fabrics = Get-AzureRmRecoveryServicesAsrFabric
 
    foreach ($Fabric in $Fabrics)
    {
        #Get containers and protected items in a container
        $Containers = Get-AzureRmRecoveryServicesAsrProtectionContainer -Fabric $Fabric
        foreach ($Container in $Containers)
        {
            $items = Get-AzureRmRecoveryServicesAsrReplicationProtectedItem -ProtectionContainer $Container
            foreach ($item in $items)
            {
                write-host "Getting replication information for workload $($item.FriendlyName)"

                #Initialize an empty error array for capturing error(s) of each protected item
                $ReplicationErrorMessages = New-Object System.Collections.ArrayList
                foreach ($ASRerror in $item.ReplicationHealthErrors)
                {
                    $ReplicationErrorMessages.Add($ASRerror.ErrorMessage) | Out-Null
                }

                try
                {
                    $object = New-Object System.Object
                    $object | Add-Member -type NoteProperty –Name FriendlyName –Value $item.FriendlyName
                    $object | Add-Member -type NoteProperty –Name ActiveLocation –Value $item.ActiveLocation
                    $object | Add-Member -type NoteProperty –Name ProtectionState –Value $item.ProtectionState
                    $object | Add-Member -type NoteProperty –Name ReplicationHealth –Value $item.ReplicationHealth
                    $object | Add-Member -type NoteProperty –Name ReplicationErrorMessages –Value $ReplicationErrorMessages
                    $object | Add-Member -type NoteProperty –Name LastSuccessfulFailoverTime –Value $item.LastSuccessfulFailoverTime
                    $object | Add-Member -type NoteProperty –Name LastSuccessfulTestFailoverTime –Value $item.LastSuccessfulTestFailoverTime
                    $object | Add-Member -type NoteProperty –Name PolicyFriendlyName –Value $item.PolicyFriendlyName
                    $object | Add-Member -type NoteProperty -Name PrimaryFabricFriendlyName -Value $item.PrimaryFabricFriendlyName
                    $object | Add-Member -type NoteProperty -Name RecoveryAzureVMSize -Value $item.RecoveryAzureVMSize
                    $object | Add-Member -type NoteProperty -Name RecoveryFabricFriendlyName -Value $item.RecoveryFabricFriendlyName
                    $object | Add-Member -type NoteProperty -Name VaultName -Value $VaultObj.Name
                    $adsObjects.Add($object) | Out-Null
                }
                catch
                {
                    Write-Warning "An error occured while processing data for workload $($item.FriendlyName) - $($_.Exception.Message)"
                }
            }
        }
    }   
    #Remove vault settings file
    rm -Path $VaultFileLocation.FilePath
}

$filePath = Read-Host -Prompt 'Where do you want to save to? (default: c:\etc\azurereplicationreport.csv)'
if ($filePath -eq "")
{
    $filePath = "c:\etc\azurereplicationreport.csv"
}
write-host "Saving replication results to $($filePath)"

#Clear the contents of CSV file
try { Clear-Content -Path $filePath -ErrorAction SilentlyContinue } catch { }
 
$adsObjects | Export-Csv -Path $filePath -Force -NoTypeInformation