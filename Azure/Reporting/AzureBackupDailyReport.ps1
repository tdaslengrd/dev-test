﻿Login-AzureRmAccount

$adsObjects = New-Object System.Collections.ArrayList
$vaults = Get-AzureRmRecoveryServicesVault

foreach ($vault in $vaults)
{
    Write-Host "Processing vault $($vault.Name)."

    $vaultcontext = Get-AzureRmRecoveryServicesVault -Name $($vault.Name)
    Set-AzureRmRecoveryServicesVaultContext -Vault $vaultcontext

    $Containers = Get-AzureRmRecoveryServicesBackupContainer -ContainerType AzureVM -Status Registered
    foreach ($vm in $Containers)
    {
        $vmContainer = Get-AzureRmRecoveryServicesBackupContainer -ContainerType AzureVM -Status Registered -FriendlyName $vm.FriendlyName
        $BackupItem = Get-AzureRmRecoveryServicesBackupItem -Container $vmContainer -WorkloadType AzureVM
        $BackupItem | Select VirtualmachineId, ProtectionStatus, ProtectionState, LastBackupStatus, lastBackupTime, LatestRecoveryPoint | Out-Null

        write-host "Saving latest backup results for $($vm.FriendlyName)"
        if($vm)
        {
            try
            {
                $object = New-Object System.Object
                $object | Add-Member -type NoteProperty –Name FriendlyName –Value $vm.FriendlyName
                $object | Add-Member -type NoteProperty –Name ProtectionStatus –Value $BackupItem.ProtectionStatus
                $object | Add-Member -type NoteProperty –Name ProtectionState –Value $BackupItem.ProtectionState
                $object | Add-Member -type NoteProperty –Name LastBackupStatus –Value $BackupItem.LastBackupStatus
                $object | Add-Member -type NoteProperty –Name LastBackupTime –Value $BackupItem.LastBackupTime
                $object | Add-Member -type NoteProperty –Name LatestRecoveryPoint –Value $BackupItem.LatestRecoveryPoint
                $object | Add-Member -type NoteProperty –Name BackupVault –Value $vault.Name
                $object | Add-Member -type NoteProperty -Name ResourceGroup -Value $vault.ResourceGroupName
                $adsObjects.Add($object) | Out-Null
            }
            catch
            {
                Write-Warning "An error occured while processing data for workload $($vm) - $($_.Exception.Message)"
            }
        }
        else
        {
            Write-Warning "$($vm.FriendlyName) has no backup data."
        }
    }
}

$filePath = Read-Host -Prompt 'Where do you want to save to? (default: c:\etc\azurebackupreport.csv)'
if ($filePath -eq "")
{
    $filePath = "c:\etc\azurebackupreport.csv"
}
Write-Host "Saving backup data to" $filePath

#Clear the contents of CSV file
try { Clear-Content -Path $filePath -ErrorAction SilentlyContinue } catch { }

$adsObjects | Export-Csv -Path $filePath -Force -NoTypeInformation