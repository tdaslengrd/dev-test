﻿Login-AzureRmAccount

$adsObjects = New-Object System.Collections.ArrayList
$vaults = Get-AzureRmRecoveryServicesVault

foreach ($vault in $vaults)
{
    Write-Host "Processing vault $($vault.Name)."

    $vaultcontext = Get-AzureRmRecoveryServicesVault -Name $($vault.Name)
    Set-AzureRmRecoveryServicesVaultContext -Vault $vaultcontext

    $historicalResults = Get-AzureRmRecoveryServicesBackupJob -From (Get-Date).AddDays(-29).ToUniversalTime()

    foreach ($historicalResult in $historicalResults)
    {
        write-host "Finding results for $($historicalResult.WorkloadName)"

        try
        {
            $object = New-Object System.Object
            $object | Add-Member -type NoteProperty –Name WorkloadName –Value $historicalResult.WorkloadName
            $object | Add-Member -type NoteProperty –Name Operation –Value $historicalResult.Operation
            $object | Add-Member -type NoteProperty –Name Status –Value $historicalResult.Status
            $object | Add-Member -type NoteProperty –Name Duration –Value $historicalResult.Duration
            $object | Add-Member -type NoteProperty –Name StartTime –Value $historicalResult.StartTime
            $object | Add-Member -type NoteProperty –Name EndTime –Value $historicalResult.EndTime
            $object | Add-Member -type NoteProperty –Name VaultName –Value $vault.Name
            $object | Add-Member -type NoteProperty -Name ResourceGroup -Value $vault.ResourceGroupName
            $adsObjects.Add($object) | Out-Null
        }
        catch
        {
            Write-Warning "An error occured while processing data for workload $($historicalResult.WorkloadName) - $($_.Exception.Message)"
        }
    }
}        

$filePath = Read-Host -Prompt 'Where do you want to save to? (default: c:\etc\azurebackuphistoricalreport.csv)'
if ($filePath -eq "")
{
    $filePath = "c:\etc\azurebackuphistoricalreport.csv"
}
write-host "Saving historical backup results to $($filePath)"

#Clear the contents of CSV file
try { Clear-Content -Path $filePath -ErrorAction SilentlyContinue } catch { }

$adsObjects | Export-Csv -Path $filePath -Force -NoTypeInformation