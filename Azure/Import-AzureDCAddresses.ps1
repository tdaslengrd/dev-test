﻿function Split-Array
{
    param($inArray,[int]$parts,[int]$size)
  
    if ($parts)
    {
        $PartSize = [Math]::Ceiling($inArray.count / $parts)
    } 
    if ($size)
    {
        $PartSize = $size
        $parts = [Math]::Ceiling($inArray.count / $size)
    }

    $outArray = @()
    for ($i=1; $i -le $parts; $i++)
    {
        $start = (($i-1)*$PartSize)
        $end = (($i)*$PartSize) - 1
        if ($end -ge $inArray.count)
        {
            $end = $inArray.count
        }
        $outArray+=,@($inArray[$start..$end])
    }
    return ,$outArray
}

# Sign-in with Azure account credentials
Login-AzureRmAccount

# Select Azure Subscription
$subscriptionId = 
    (Get-AzureRmSubscription |
     Out-GridView `
        -Title "Select an Azure Subscription …" `
        -PassThru).SubscriptionId

Select-AzureRmSubscription `
    -SubscriptionId $subscriptionId

# Select Azure Resource Group
$rgName =
    (Get-AzureRmResourceGroup |
     Out-GridView `
        -Title "Select an Azure Resource Group …" `
        -PassThru).ResourceGroupName

# Download current list of Azure Public IP ranges
# See this link for latest list

$downloadUri = "https://www.microsoft.com/en-in/download/confirmation.aspx?id=41653"

$downloadPage = Invoke-WebRequest -Uri $downloadUri

$xmlFileUri = ($downloadPage.RawContent.Split('"') -like "https://*PublicIps*")[0]

$response = Invoke-WebRequest -Uri $xmlFileUri

# Get list of regions & public IP ranges
[xml]$xmlResponse = [System.Text.Encoding]::UTF8.GetString($response.Content)
$regions = $xmlResponse.AzurePublicIpAddresses.Region

# Select Azure regions for which to define NSG rules
$selectedRegions =
    $regions.Name |
    Out-GridView `
        -Title "Select Azure Datacenter Regions …" `
        -PassThru

$ipRange = ( $regions | where-object Name -In $selectedRegions ).IpRange

# Build NSG rules
$rules = @()
$rulePriority = 2000

ForEach ($subnet in $ipRange.Subnet)
{
    $ruleName = "Allow_Azure_In_" + $subnet.Replace("/","-")
    
    $rules += 
        New-AzureRmNetworkSecurityRuleConfig `
            -Name $ruleName `
            -Description "Allow inbound traffic from Azure $subnet" `
            -Access Allow `
            -Protocol * `
            -Direction Inbound `
            -Priority $rulePriority `
            -SourceAddressPrefix VirtualNetwork `
            -SourcePortRange * `
            -DestinationAddressPrefix "$subnet" `
            -DestinationPortRange *

    $rulePriority++
}

# Check if there are more than 200 rules
if ($rules.count -gt 200)
{
    write-host "Array is greater than 200 entries!"
    $SplitCount = [int][Math]::Ceiling($rules.count/200)
    write-host "Splitting entries into $SplitCount arrays."
    $ArrayCollection = Split-array -inArray $rules -parts $SplitCount
}

$i = 1

ForEach($Array in $ArrayCollection)
{
    # Set Azure region in which to create NSG
    $location = "australiaeast"

    # Create Network Security Group
    $nsgName = "AllowAzureInBound-$selectedRegions-$i"
    write-host "Creating $nsgName network security group."

    $nsg = 
        New-AzureRmNetworkSecurityGroup `
           -Name "$nsgName" `
           -ResourceGroupName $rgName `
           -Location $location `
           -SecurityRules $Array
    $i++
}