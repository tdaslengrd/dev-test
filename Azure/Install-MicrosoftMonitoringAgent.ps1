﻿# Enter the workspace Id and Key from the Azure Log Analytics subscription
$WorkspaceId = "e444ced6-0eea-4a7b-af47-df4ec6e40b97"
$WorkspaceKey = "Mess4x8aC5MTT5tC2yLf5ggJjKRDXu2AbFPI4AfMlaYUKtKsmgZHPsljWgbvbuK3rwmDE2H+9ZgFCY/vXwMx+w=="

Write-Output "Downloading Microsoft Monitoring Agent..."
$url = "https://tdaslstoraccaue.blob.core.windows.net/pubshare/MMASetup-AMD64.exe"
$output = "$env:Temp\MMASetup-AMD64.exe"
$start_time = Get-Date
Try
{
    Invoke-WebRequest -Method Get -Uri $url -OutFile $output
    Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

    Write-Output "Installing..."
    Start-Process $output -ArgumentList "/Q:A /R:N /C:`"setup.exe /qn ADD_OPINSIGHTS_WORKSPACE=1 OPINSIGHTS_WORKSPACE_ID=$WorkspaceId OPINSIGHTS_WORKSPACE_KEY=$WorkspaceKey AcceptEndUserLicenseAgreement=1`"" -Wait
    Write-Output "Finished installing Microsoft Monitoring Agent."
}
Catch
{
    Write-Output "An error occurred:"
    Write-Error $_
}