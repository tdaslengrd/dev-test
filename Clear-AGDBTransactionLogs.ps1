﻿#$ServerName = "1011LABSQL1401\OM"
#$DatabaseName = "OperationsManager"
#$AGName = "OM_12-AG"

$SQLQueryString =
@"
    USE master
    GO
    ALTER AVAILABILITY GROUP [$($AGName)] REMOVE DATABASE $($DatabaseName);
    GO
    ALTER DATABASE $($DatabaseName) SET RECOVERY SIMPLE
    GO
    DECLARE @SQLStatement VARCHAR(2000) 
    SET @SQLStatement = '\\1011labfsclap01\SQLBackup\$($DatabaseName)_' + CONVERT(nvarchar(30), GETDATE(), 110) +'.bak'
    BACKUP DATABASE $($DatabaseName) TO DISK = @SQLStatement
    GO
    USE $($DatabaseName)
    DBCC shrinkfile('$($DatabaseName)',1) /*/ Shrink down to 1 MB */
    GO
    USE master
    alter database $($DatabaseName) SET RECOVERY FUll
    GO
    DECLARE @SQLStatement VARCHAR(2000) 
    SET @SQLStatement = '\\1011labfsclap01\SQLBackup\$($DatabaseName)_' + CONVERT(nvarchar(30), GETDATE(), 110) +'.bak'
    BACKUP DATABASE $($DatabaseName) TO DISK = @SQLStatement
    GO
    ALTER AVAILABILITY GROUP [$($AGName)] ADD DATABASE $($DatabaseName);
    GO
"@

function Clear-AGDBTransactionLogs
{
    param( [string]$ServerName, [string]$DatabaseName, [string]$AGName )

    if (!$ServerName  -or !$DatabaseName -or !$AGName)
    {
        write-host "Variables not passed through!"
        write-host "This script is designed to automate the tak of clearing a transction log which is full on a database which is part of a SQL Avaliability Group."
        write-host "You will need to pass the SQL Server Name to connect to, target Database you want to clear the transaction log on and the Avaliability Group name for the Always On group."
        write-host 'For example, to target a SQL instance, Clear-AGDBTransactionLogs -ServerName "1011LABSQL1401\OM" -DatabaseName "OperationsManager" -AGName "OM_12-AG"'
        write-host 'For example, to target a SQL instance, Clear-AGDBTransactionLogs -ServerName "1011LABSQL1401" -DatabaseName "vConnect" -AGName "5Nine-AG"'
    }
    else
    {
        $null = [Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo")
        $SqlServer = New-Object Microsoft.SqlServer.Management.Smo.Server($ServerName)
        $data = $SqlServer.AvailabilityGroups.DatabaseReplicaStates | Select-Object AvailabilityReplicaServerName, AvailabilityDatabaseName, ReplicaRole
        $TargetDatabaseServerName = $data | Where {$_.AvailabilityDatabaseName -eq $DatabaseName -and $_.ReplicaRole -ne "Secondary"} | Foreach {$_.AvailabilityReplicaServerName} //Finds primary database server for AG

        Invoke-Sqlcmd -Query $SQLQueryString -ServerInstance $TargetDatabaseServerName \\Runs above T-SQL command against the primary database server found above
    }
}