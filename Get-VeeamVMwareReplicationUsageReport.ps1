﻿#######################
# PowerShell Add-In's #
#######################
Add-PSSnapin *Veeam*
Add-PSSnapin *VMware*

###############################################
# Global Variables - Change these accordingly #
###############################################
# Set the below bool variables to 0 to enable feature
[bool]$diagnose = 0
[bool]$sendemail = 1
[bool]$writetosql = 1

# Email server settings
$smtpServer = "internalmail.ico.com.au"
$fromaddress = "backup_data@ico.com.au"
$emailrecipient = "taz.razak@avnet.com" # For multiple recipients, provide a comma separated string

# SQL connection information
$sqlselect = 0
$sqledit = 1
$aregconnectionString = "Server=10.100.0.111;Database=assetregisterSQL;Uid=sa;Pwd=FT&I9Hmj;"

# Veeam connection settings
$veeamconnectionString = "Server=192.168.100.4;Database=VeeamBackup;Uid=sa;Pwd=FT&I9Hmj;"

# VMware vCenter connection details
$vcserver = "vc2.ico.com.au"
$vcuser = ""
$vcpw = ""

############################
# Initialisation Variables #
############################
$servers = @()

####################################
# Connect to VMware vSphere server #
####################################
Connect-VIServer -Server $vcserver -User $vcuser -Password $vcpw


#####################
# Program functions #
#####################
function execute_sql([string]$sql, [string]$connectionstring, [int]$type)
{
	$connection = New-Object System.Data.SqlClient.SqlConnection
	$connection.ConnectionString = $connectionstring
	$connection.Open()
	$command = $connection.CreateCommand()
	$command.CommandText = $sql
	if ($type -eq $sqlselect) {
		$result = $command.ExecuteScalar()
		$connection.Close()
		return $result
	}
	if ($type -eq $sqledit) {
		$command.executenonquery()
	}
	$connection.Close()
}


######################################################
# Select the next JobID from the BackupDataJob table #
######################################################
$sql = "select max(jobid) from BackupDataJob"
$jobid = (execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqlselect) + 1


##############################################################################
# Get VMware virtual machines where the Name of each object is like _replica #
##############################################################################
$servers = Get-VM | Select-Object Name,ProvisionedSpaceGB,Host | Where { $_.Name -like '*_replica' }


##########################################################################################################################################
# Main program loop, checks each server in array and performs tests against server to identify charastics and then inserts data into SQL #
##########################################################################################################################################
foreach ($server in $servers)
{
    # Set the server name
    $vmname = $server.Name

    # Get the CustomerID based on Name from VMware
    $numberextract = $vmname -match "([0-9])\d+"
    $cid = $matches[0]

    # Round down the disk size retrived from VMware to 2 decimal points
    $sizeGB = [math]::Round($server.ProvisionedSpaceGB,2)

    # Connect to the assetregisterSQL database and get all SAP contract ID's based on VMware list above
	$sql = "SELECT contract_backup_sap FROM Machines WHERE [Machine Name] = '" + $server.Name + "'"		
	$contract_backup = execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqlselect

	# Check if the SAP ID is empty or null
	if (!$contract_backup -or [System.DBNull]::Value.Equals($contract_backup))
    {
		$contract_backup = "No Contract Number"
		$errors += "`n`r$($server.Name) does not have a SAP 'Backup' Contract ID!"
	}

	$sql = "insert into backupdatausage (jobid,machinename,clientid,GB,path,contract_backup_sap) values ($jobid,'$($server.Name)','$cid',$sizeGB,'$($server.Host)','$contract_backup')"

	# Writes above data to SQL database
	if (!$writetosql)
	{
		execute_sql -sql $sql -connectionstring $aregconnectionString -type $sqledit
	}

    # Output the Insert SQL string which will be used to store data in SQL
    if (!$diagnose)
    {
        write-host "VM Name - " $vmname
        write-host "Customer ID - " $cid
        write-host "Storage Allocated in GB - " $sizeGB
        write-host "SAP Contract ID - " $contract_backup
        write-host "Errors - "
        foreach ($error in $errors)
        {
            write-host $error
        }
        write-host "SQL Insert String - " $sql
    }
}

# Send email report for errors encountered during execution
if (!$sendemail)
{
     $msg = new-object Net.Mail.MailMessage
     $smtp = new-object Net.Mail.SmtpClient($smtpServer)

     #Email structure
     $msg.From = $fromaddress
     $msg.To.Add($emailrecipient)
     $msg.subject = "Backup billing errors"
     $msg.body = $errors

     #Sending email
     $smtp.Send($msg)

	 write-host "Email sent" 
}