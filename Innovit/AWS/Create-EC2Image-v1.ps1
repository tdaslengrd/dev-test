﻿#Access Keys
$accessKeyID="AKIAIYVCAVFHFNWABDNA"
$secretAccessKey="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"
$accountID = "358884484514"

#Account Specific Settings
$region = "us-east-1"
$array = @("houston04.innovit.com") # Name of servers to be restarted
$longTime =  Get-Date -Format "yyyy-MM-dd_HH-mm-ss" # Get current time into a string
$logFilename = "C:\etc\Scripts\AWS\Logs-AMI\$instanceName-AMI-" + $longTime + ".log"

# Create an AMI from a running instance by using the instance's name tag, tag the resulting AMI and all snapshots with a meaningful tag
Set-AWSCredentials -AccessKey $accessKeyID -SecretKey $secretAccessKey -StoreAs myAWScredentials
Set-AWSCredentials -StoredCredentials myAWScredentials

Try
{
    foreach ($instanceName in $array)
    {
        $instanceID = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances #Get instance ID
        $deptTagValue = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances | Select -ExpandProperty Tags | where { $_.Key -eq "DepartmentAllocation" } | Select Value | foreach { $_.Value }
 
        $tagDesc = "Created by " + $MyInvocation.MyCommand.Name + " on " + $longTime # Make a nice string for the Description tag
        $amiName = $instanceName + " AMI " + $longTime # Make a name for the AMI
 
        $amiID = New-EC2Image -InstanceId $instanceID.InstanceId -Description $tagDesc -Name $amiName -NoReboot:$true # Create the AMI, rebooting the instance in the process
 
        Start-Sleep -Seconds 60 # For some reason, it can take some time for subsequent calls to Get-EC2Image to return all properties, especially for snapshots. So we wait
 
        $shortTime = Get-Date -Format "yyyy-MM-dd" # Shorter date for the name tag
        $tagName = $instanceName + " AMI " + $shortTime # Sting for use with the name TAG -- as opposed to the AMI name, which is something else and set in New-EC2Image
 
        New-EC2Tag -Resources $amiID -Tags @( @{ Key = "Name" ; Value = $tagName}, @{ Key = "Description"; Value = $tagDesc }, @{ Key = "DepartmentAllocation"; Value = $deptTagValue } ) # Add tags to new AMI
     
        $amiProperties = Get-EC2Image -ImageIds $amiID # Get Amazon.EC2.Model.Image
  
        $amiBlockDeviceMapping = $amiProperties.BlockDeviceMapping # Get Amazon.Ec2.Model.BlockDeviceMapping
  
        Foreach ($ebsvolume in $amiBlockDeviceMapping.ebs)
        {
            New-EC2Tag -Resources $ebsvolume.SnapshotID -Tags @( @{ Key = "Name" ; Value = $amiName}, @{ Key = "DepartmentAllocation"; Value = $deptTagValue } ) # Add tags to snapshots associated with the AMI using Amazon.EC2.Model.EbsBlockDevice 
        }
    }
}
Catch
{
    "[CATCH] Errors found during attempt:`n$_" | Out-File -FilePath $logFilename -Append
}
Finally
{
    "Created AMI" + " " + $amiID + " " + $amiName | Out-File -FilePath $logFilename -Append
    Clear-AWSCredentials
}