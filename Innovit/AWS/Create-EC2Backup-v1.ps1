﻿#Access Keys
$accessKeyID="AKIAIYVCAVFHFNWABDNA"
$secretAccessKey="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"
$accountID = "358884484514"

$instanceNames = @("houston04.innovit.com")
$Tag = 'DailyBackup'
$Value = 'Yes'
$RetentionDays = 7
$backupDate =  Get-Date -Format "yyyy-MM-dd_HH-mm-ss" # Get current time into a string
$logFilename = "C:\etc\Scripts\AWS\Logs\$instanceName-Snapshot-" + $backupDate + ".log"

Set-AWSCredentials -AccessKey $accessKeyID -SecretKey $secretAccessKey -StoreAs myAWScredentials
Set-AWSCredentials -StoredCredentials myAWScredentials
$ownerID = @(get-ec2securitygroup -GroupNames "default")[0].OwnerId

Try
{
    foreach ($instanceName in $instanceNames)
    {
        echo "Going to backup - $instanceName"
        "[INFO] Going to backup - $instanceName." | Out-File -FilePath $logFilename -Append

        $deptTagValue = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances | Select -ExpandProperty Tags | where { $_.Key -eq "DepartmentAllocation" } | Select Value | foreach { $_.Value }
        $snapshotName = $instanceName + " Snapshot " + $longTime

        #Retrieve all volumes that should be backed up
        $instance = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances
        $instanceID = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty Instances | Select InstanceId | Foreach {$_.InstanceId}
        $tags = $instance | Select -ExpandProperty Tags | where { $_.Key -eq $Tag -and $_.Value -eq $Value }
        $backupVolumes = Get-EC2Volume | Select Attachments | Foreach {$_.Attachments} | where {$_.InstanceID -eq $instanceID} | Select VolumeID | Foreach {$_.VolumeID}

        #Backup each volume and apply tag information to the volume and snapshot
        Foreach ($backupVolume in $backupVolumes)
        {
            echo "Snapshotting volume - $backupVolume"
            "[INFO] Snapshotting volume - $backupVolume." | Out-File -FilePath $logFilename -Append

            $snapshot = New-Ec2snapshot -VolumeId $backupVolume -Description "Backup for $instanceName - $backupDate"

            Start-Sleep -Seconds 60 # For some reason, it can take some time for subsequent calls to Get-EC2Image to return all properties, especially for snapshots. So we wait

            New-EC2Tag -Resources $snapshot.SnapshotId -Tags @( @{ Key = "Name"; Value = $snapshotName}, @{ Key = "BackupDate"; Value = $backupDate }, @{ Key = "DepartmentAllocation"; Value = $deptTagValue } ) # Add tags to new AMI
        }

        echo "Snapshot succeeded for $instanceName"
 
        #Remove old backups 
        echo "Checking retained snapshots."
        "[INFO] Checking retained snapshots." | Out-File -FilePath $logFilename -Append
        echo "Set to keep $RetentionDays snapshots."
        "[INFO] Set to keep $RetentionDays snapshots." | Out-File -FilePath $logFilename -Append
 
        $Snapshots = Get-EC2Snapshot -OwnerId $ownerID -Filter @{name='tag:BackupDate'}
        $SnapshotCount = $Snapshots.Count
        echo "Found $SnapshotCount snapshots."
        "[INFO] Found $SnapshotCount snapshots." | Out-File -FilePath $logFilename -Append

        foreach ($Snapshot in $Snapshots)
        { 
            $Retention = ([DateTime]::Now).AddDays(-$RetentionDays)
            if ([DateTime]::Compare($Retention, $Snapshot.StartTime) -gt 0)
            {
                echo "Removing $snapshot.SnapshotId"
                "[INFO] Removing $snapshot.SnapshotId." | Out-File -FilePath $logFilename -Append
                Remove-EC2Snapshot -SnapshotId $snapshot.SnapshotId -Force 
            } 
        }
    }
}
Catch
{
    "[CATCH] Errors found during attempt:`n$_" | Out-File -FilePath $logFilename -Append
}
Finally
{
    Clear-AWSCredentials
    "[INFO] Script execution completed, check output above for any warning or notices." | Out-File -FilePath $logFilename -Append
}