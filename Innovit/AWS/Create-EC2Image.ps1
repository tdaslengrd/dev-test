﻿#AWS Account Specific Settings
$accessKeyID="AKIAIYVCAVFHFNWABDNA"
$secretAccessKey="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"
$accountID = "358884484514"
$region = "us-east-1"
$array = @("aws-nv-lic.innovit.com") # Name of servers to be restarted
$longTime =  Get-Date -Format "yyyy-MM-dd_HH-mm-ss" # Get current time into a string
#$logFilename = "C:\etc\Scripts\AWS\Logs-AMI\$instanceName-AMI-" + $longTime + ".log"
#$backupDate =  Get-Date -Format "yyyy-MM-dd_HH-mm-ss" # Get current time into a string
$RetentionDays = "7"
$TestDelete = "0"

#Mail server settings
$SMTPServer = "smtp.ico.com.au"
$SMTPPort = "25"
$Username = "cloud@atscloud247.com"
$Password = "D9b#D312"
$From = "aws-nv-lic-innovit@atscloud247.com"
$to = "support@techdataaucloud.atlassian.net"
$cc = "backupemails@innovit.com"
#$attachment = ""
$body =""
$SendEmail = "1"

Try
{
    # Create an AMI from a running instance by using the instance's name tag, tag the resulting AMI and all snapshots with a meaningful tag
    Set-AWSCredentials -AccessKey $accessKeyID -SecretKey $secretAccessKey -StoreAs myAWScredentials
    Set-AWSCredentials -StoredCredentials myAWScredentials
    $ownerID = @(get-ec2securitygroup -GroupNames "default")[0].OwnerId

    Write-Host "[INFO] Starting EC2 image job."
    $body += "[INFO] Starting EC2 image job. `n"

    foreach ($instanceName in $array)
    {
        if ($TestDelete -eq '0')
            {
                $instanceID = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances #Get instance ID
                $deptTagValue = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances | Select -ExpandProperty Tags | where { $_.Key -eq "DepartmentAllocation" } | Select Value | foreach { $_.Value }
 
                $tagDesc = "Created by " + $MyInvocation.MyCommand.Name + " on " + $longTime # Make a nice string for the Description tag
                $amiName = $instanceName + " AMI " + $longTime # Make a name for the AMI
 
                Write-Host "[INFO] Creating AMI, this can take a few minutes to complete."
                $body += "[INFO] Creating AMI, waiting 2 minutes for the process to complete. `n"
                $amiID = New-EC2Image -InstanceId $instanceID.InstanceId -Description $tagDesc -Name $amiName -NoReboot:$true # Create the AMI, rebooting the instance in the process
 
                Start-Sleep -Seconds 120 # For some reason, it can take some time for subsequent calls to Get-EC2Image to return all properties, especially for snapshots. So we wait
 
                write-host "[INFO] Created AMI $amiID - $amiName"
                $body += "[INFO] Created AMI " + $amiID + " - " + $amiName + ". `n"

                $shortTime = Get-Date -Format "yyyy-MM-dd" # Shorter date for the name tag
                $tagName = $instanceName + " AMI " + $shortTime # Sting for use with the name TAG -- as opposed to the AMI name, which is something else and set in New-EC2Image
 
                New-EC2Tag -Resources $amiID -Tags @( @{ Key = "Name" ; Value = $tagName}, @{ Key = "Description"; Value = $tagDesc }, @{ Key = "DepartmentAllocation"; Value = $deptTagValue }, @{ Key = "ImageDate"; Value = $longTime }, @{ Key = "InstanceName"; Value = $instanceName } ) # Add tags to new AMI
     
                $amiProperties = Get-EC2Image -ImageIds $amiID # Get Amazon.EC2.Model.Image
  
                $amiBlockDeviceMapping = $amiProperties.BlockDeviceMapping # Get Amazon.Ec2.Model.BlockDeviceMapping
  
                Foreach ($ebsvolume in $amiBlockDeviceMapping.ebs)
                {
                    New-EC2Tag -Resources $ebsvolume.SnapshotID -Tags @( @{ Key = "Name" ; Value = $amiName}, @{ Key = "DepartmentAllocation"; Value = $deptTagValue }, @{ Key = "ImageDate"; Value = $longTime }, @{ Key = "ImageDate"; Value = $longTime }, @{ Key = "InstanceName"; Value = $instanceName } ) # Add tags to snapshots associated with the AMI using Amazon.EC2.Model.EbsBlockDevice 
                }
            }

        #Remove old backups 
        Write-Host "[INFO] Checking retained images."
        $body += "[INFO] Checking retained images. `n"
        Write-Host "[INFO] Set to keep $RetentionDays images."
        $body += "[INFO] Set to keep " + $RetentionDays + " images. `n"
 
        $Snapshots = Get-EC2Image -Owner $ownerID | Where-Object {$_.Tag.Value -eq $instanceName}
        $OtherSnapshots = Get-EC2Image -Owner $ownerID
        $SnapshotCount = $Snapshots.Count
        $OtherSnapshotsCount = $OtherSnapshots.Count

        Write-Host "[INFO] Found $SnapshotCount images."
        $body += "[INFO] Found " + $SnapshotCount + " images. `n `n"
        if ($OtherSnapshotsCount -ne 0)
        {
            Write-Host "[NOTICE] We also found $OtherSnapshotsCount images which were taken outside of this job."
            $body += "[NOTICE] We also found " + $OtherSnapshotsCount + " images which were taken outside of this job. `n"
            Write-Host "[NOTICE] You should consider cleaning these up manually."
            $body += "[NOTICE] You should consider cleaning these up manually. `n"
            foreach ($OtherSnapshot in $OtherSnapshots)
            {
                Write-Host "[NOTICE] "$OtherSnapshot.Name"("$OtherSnapshot.ImageId")"
                $body += "[NOTICE] " + $OtherSnapshot.Name + " (" + $OtherSnapshot.ImageId + "). `n"
            }
            
        }
        $body += " `n"

        foreach ($Snapshot in $Snapshots)
        {
            $Retention = ([DateTime]::Now).AddDays(-$RetentionDays).ToString("yyyy-MM-ddTHH\:mm\:ss.fff")
            if ([DateTime]::Compare($Retention, $Snapshot.CreationDate) -gt 0)
            {
                Write-Host "[INFO] Removing $snapshot.ImageId"
                $body += "[INFO] Removing " + $snapshot.ImageId + ". `n"

                Unregister-EC2Image -ImageId $snapshot.ImageId -Force
            } 
        }

        #Remove old AMI snapshots
        Write-Host "[INFO] Checking retained snaphots created for ami."
        $body += "[INFO] Checking retained snaphots created for ami. `n"
        Write-Host "[INFO] Set to keep $RetentionDays snapshots."
        $body += "[INFO] Set to keep " + $RetentionDays + " snapshots. `n"

        $amiSnapshots = Get-EC2Snapshot -OwnerId $ownerID | Where-Object {$_.Tag.Value -eq $instanceName -and $_.Tag.Key -eq 'ImageDate'} | Select Description | ForEach-Object {$_.Description}
        $amiSnapshotCount = $amiSnapshots.Count
        Write-Host "[INFO] Found $amiSnapshotCount unique snapshots."
        $body += "[INFO] Found " + $amiSnapshotCount + " unique snapshots. `n"

        foreach ($amiSnapshot in $amiSnapshots)
        {
            $amiSnapshotsDetails = Get-EC2Snapshot -OwnerId $ownerID | Where-Object {$_.Description -eq $amiSnapshot}
            $amiSnapshotsDetailsCount = $amiSnapshotsDetails.Count
            foreach ($amiSnapshotDetails in $amiSnapshotsDetails)
            {
                Write-Host "[INFO] Checking snapshot $amiSnapshotDetails.SnapshotId for instance $instanceName."
                $body += "[INFO] Checking snapshot " + $amiSnapshotDetails.SnapshotId + " for instance " + $instanceName + ". `n"

                $amiRetention = ([DateTime]::Now).AddDays(-$RetentionDays)
                if ([DateTime]::Compare($amiRetention, $amiSnapshotDetails.StartTime) -gt 0)
                {
                    Write-Host "[INFO] Removing $amiSnapshotDetails.SnapshotId."
                    $body += "[INFO] Removing " + $amiSnapshotDetails.SnapshotId + ". `n"

                    Remove-EC2Snapshot -SnapshotId $amiSnapshotDetails.SnapshotId -Force 
                }
            }
        }

        #Send email
        Write-Host "[INFO] Sucessfully complete image job."
        $body += "[INFO] Sucessfully complete image job. `n"

        $subject = "AWS EC2 Image - Success"
        $message = New-Object System.Net.Mail.MailMessage
        $message.subject = $subject
        $message.body = $body
        $message.to.add($to)
        $message.cc.add($cc)
        $message.from = $from
        #$message.attachments.add($attachment)
        $smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
        $smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);

        if ($SendEmail -eq "1")
        {
            $smtp.Send($message)
            write-host "[INFO] Mail Sent"
        }
        else
        {
            write-host "[INFO] Configured not to send mail report!"
        }        
    }
}
Catch
{
    write-host "[CATCH] Errors found during attempt:`n$_"
    $body = "[CATCH] Errors found during attempt:`n$_"
    $subject = "AWS EC2 Image - Failure"
    $message.subject = $subject
    $message.body = $body
    $message.to.add($to)
    $message.cc.add($cc)
    $message.from = $from
    #$message.attachments.add($attachment)
    $smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
    $smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);

    if ($SendEmail -eq "1")
    {
        $smtp.Send($message)
        write-host "[INFO] Mail Sent"
    }
    else
    {
        write-host "[INFO] Configured not to send mail report!"
    }
}
Finally
{
    Clear-AWSCredentials
}