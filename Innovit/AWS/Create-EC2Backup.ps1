﻿#AWS Settings
$accessKeyID="AKIAIYVCAVFHFNWABDNA"
$secretAccessKey="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"
$accountID = "358884484514"
$instanceNames = @("aws-nv-lic.innovit.com")
$Tag = 'DailyBackup'
$Value = 'Yes'
$RetentionDays = 7
$backupDate =  Get-Date -Format "yyyy-MM-dd_HH-mm-ss" # Get current time into a string
$logFilename = "C:\etc\Scripts\AWS\Logs\$instanceName-Snapshot-" + $backupDate + ".log"
$TestDelete = "0"

#Mail server settings
$SMTPServer = "smtp.ico.com.au"
$SMTPPort = "25"
$Username = "cloud@atscloud247.com"
$Password = "D9b#D312"
$From = "aws-nv-lic-innovit@atscloud247.com"
#$to = "taz.razak@avnet.com"
$to = "support@techdataaucloud.atlassian.net"
$cc = "backupemails@innovit.com"
#$attachment = ""
$body =""
$SendEmail = "1"

Try
{
    Set-AWSCredentials -AccessKey $accessKeyID -SecretKey $secretAccessKey -StoreAs myAWScredentials
    Set-AWSCredentials -StoredCredentials myAWScredentials
    $ownerID = @(get-ec2securitygroup -GroupNames "default")[0].OwnerId

    Write-Host "[INFO] Starting EC2 snapshot job."
    $body += "[INFO] Starting EC2 snapshot job. `n"

    foreach ($instanceName in $instanceNames)
    {
        if ($TestDelete -eq '0')
        {
            Write-Host "[INFO] Going to backup - $instanceName."
            $body += "[INFO] Going to backup - " + $instanceName + ". `n"

            $deptTagValue = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances | Select -ExpandProperty Tags | where { $_.Key -eq "DepartmentAllocation" } | Select Value | foreach { $_.Value }
            $snapshotName = $instanceName + " Snapshot " + $longTime

            #Retrieve all volumes that should be backed up
            $instance = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty instances
            $instanceID = Get-EC2Instance -Filter @{name='tag:Name'; values=$instanceName} | Select -ExpandProperty Instances | Select InstanceId | Foreach {$_.InstanceId}
            $tags = $instance | Select -ExpandProperty Tags | where { $_.Key -eq $Tag -and $_.Value -eq $Value }
            $backupVolumes = Get-EC2Volume | Select Attachments | Foreach {$_.Attachments} | where {$_.InstanceID -eq $instanceID} | Select VolumeID | Foreach {$_.VolumeID}

            #Backup each volume and apply tag information to the volume and snapshot
            Foreach ($backupVolume in $backupVolumes)
            {
                Write-Host "[INFO] Snapshotting volume - $backupVolume"
                $body += "[INFO] Snapshotting volume - " + $backupVolume + ". `n"

                $snapshot = New-Ec2snapshot -VolumeId $backupVolume -Description "Backup for $instanceName - $backupDate"

                Write-Host "[INFO] Waiting for snapshot to complete."
                $body += "[INFO] Waiting for snapshot to complete. `n"
                Start-Sleep -Seconds 120 # For some reason, it can take some time for subsequent calls to Get-EC2Image to return all properties, especially for snapshots. So we wait

                New-EC2Tag -Resources $snapshot.SnapshotId -Tags @( @{ Key = "Name"; Value = $snapshotName}, @{ Key = "BackupDate"; Value = $backupDate }, @{ Key = "DepartmentAllocation"; Value = $deptTagValue }, @{ Key = "InstanceName"; Value = $instanceName } ) # Add tags to new AMI
            }

            Write-Host "[INFO] Snapshot succeeded for $instanceName"
            $body += "[INFO] Snapshot succeeded for " + $instanceName + ". `n"
 
            $OtherSnapshots = Get-EC2Snapshot -OwnerId $ownerID | Where-Object {$_.Name -notcontains 'AMI'}
        }

        #Remove old backups
        Write-Host "[INFO] Checking retained snapshots."
        $body += "[INFO] Checking retained snapshots. `n"
        Write-Host "[INFO] Set to keep $RetentionDays snapshots."
        $body += "[INFO] Set to keep " + $RetentionDays + " snapshots. `n"
 
        $Snapshots = Get-EC2Snapshot -OwnerId $ownerID | Where-Object {$_.Tag.Value -eq $instanceName -and $_.Tag.Key -eq 'BackupDate'} | Select Description | ForEach-Object {$_.Description}
        $SnapshotCount = $Snapshots.Count
        Write-Host "[INFO] Found $SnapshotCount unique snapshots."
        $body += "[INFO] Found " + $SnapshotCount + " unique snapshots. `n"

        foreach ($Snapshot in $Snapshots)
        {
            $SnapshotsDetails = Get-EC2Snapshot -OwnerId $ownerID | Where-Object {$_.Description -eq $Snapshot}
            $SnapshotsDetailsCount = $SnapshotsDetails.Count
            foreach ($SnapshotDetails in $SnapshotsDetails)
            {
                Write-Host "[INFO] Checking snapshot $SnapshotDetails.SnapshotId for instance $instanceName."
                $body += "[INFO] Checking snapshot " + $SnapshotDetails.SnapshotId + " for instance " + $instanceName + ". `n"

                $Retention = ([DateTime]::Now).AddDays(-$RetentionDays)
                if ([DateTime]::Compare($Retention, $SnapshotDetails.StartTime) -gt 0)
                {
                    Write-Host "[INFO] Removing $SnapshotDetails.SnapshotId."
                    $body += "[INFO] Removing " + $SnapshotDetails.SnapshotId + ". `n"

                    Remove-EC2Snapshot -SnapshotId $SnapshotDetails.SnapshotId -Force 
                }
            }
        }

        Write-Host "[INFO] Sucessfully complete snapshot job."
        $body += "[INFO] Sucessfully complete snapshot job. `n"

        $subject = "AWS EC2 Snapshot - Success"
        $message = New-Object System.Net.Mail.MailMessage
        $message.subject = $subject
        $message.body = $body
        $message.to.add($to)
        $message.cc.add($cc)
        $message.from = $from
        #$message.attachments.add($attachment)
        $smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
        $smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);

        if ($SendEmail -eq "1")
        {
            $smtp.Send($message)
            write-host "[INFO] Mail Sent"
        }
        else
        {
            write-host "[INFO] Configured not to send mail report!"
        }
    }
}
Catch
{
    write-host "[CATCH] Errors found during attempt:`n$_"
    $body = "[CATCH] Errors found during attempt:`n$_"
    $subject = "AWS EC2 Snapshot - Failure"
    $message.subject = $subject
    $message.body = $body
    $message.to.add($to)
    $message.cc.add($cc)
    $message.from = $from
    #$message.attachments.add($attachment)
    $smtp = New-Object System.Net.Mail.SmtpClient($SMTPServer, $SMTPPort);
    $smtp.Credentials = New-Object System.Net.NetworkCredential($Username, $Password);

    if ($SendEmail -eq "1")
    {
        $smtp.Send($message)
        write-host "[INFO] Mail Sent"
    }
    else
    {
        write-host "[INFO] Configured not to send mail report!"
    }
}
Finally
{
    Clear-AWSCredentials
}