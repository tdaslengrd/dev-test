New-Item -Path "HKLM:Software\Policies\Microsoft\Windows\WindowsUpdate" -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate" -Name AcceptTrustedPublisherCerts -Value "1" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate" -Name ElevateNonAdmins  -Value "1" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate" -Name TargetGroup -Value "Innovit" -Type String -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate" -Name TargetGroupEnabled  -Value "0" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate" -Name WUServer -Value "https://wsus.atscloud247.com:443"  -Type String -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate" -Name WUStatusServer -Value "https://wsus.atscloud247.com:443"  -Type String -force

New-Item -Path "HKLM:Software\Policies\Microsoft\Windows\WindowsUpdate\AU" -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name UseWUServer -Value "1" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name AUOptions -Value "4" -Type DWORD -force 
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name AUPowerManagement -Value "1" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name AutoInstallMinorUpdates -Value "1" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name DetectionFrequency -Value "10" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name DetectionFrequencyEnabled -Value "1" -Type DWORD -force 
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name IncludeRecommendedUpdates -Value "1" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name NoAUAsDefaultShutdownOption -Value "1" -Type DWORD -force 
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name NoAUShutdownOption -Value "1" -Type DWORD -force 
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name NoAutoRebootWithLoggedOnUsers -Value "0" -Type DWORD -force 
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name NoAutoUpdate -Value "0" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name RebootRelaunchTimeout -Value "10" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name RebootRelaunchTimeoutEnabled -Value "1" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name RescheduleWaitTime -Value "10" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name RescheduleWaitTimeEnabled -Value "1" -Type DWORD -force 
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name ScheduledInstallDay -Value "7" -Type DWORD -force
Set-ItemProperty -Path "HKLM:\software\policies\Microsoft\Windows\WindowsUpdate\AU" -Name ScheduledInstallTime -Value "3" -Type DWORD -force