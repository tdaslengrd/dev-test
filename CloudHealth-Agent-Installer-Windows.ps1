﻿$currentPSEPolicy = Get-ExecutionPolicy
Set-ExecutionPolicy -ExecutionPolicy Bypass

$dldir = "c:\etc\CloudHealth\"
If(!(Test-Path $dldir))
{
    New-Item -ItemType Directory -Force -Path $dldir
}

$token = Read-Host -Prompt 'Enter your CloudHealth token ID (e.g. /qn CLOUDNAME=aws CHTAPIKEY=4fe6d708-8563-7589-45t4-bef5a7825a4b)'
$source = "https://s3.amazonaws.com/remote-collector/agent/windows/13/CloudHealthAgent.exe"
$logdir = $dldir + "install.log"
$destination = $dldir + "CloudHealthAgent.exe"
$installer = $destination + " /S /v" + [char]34 + " /l* " + $logdir + " " + $token + [char]34

Install-WindowsFeature Net-Framework-Core -Verbose
Invoke-WebRequest $source -OutFile $destination
Invoke-Expression $installer

Set-ExecutionPolicy $currentPSEPolicy
pause